%% Modeling a series interconnection of R, L, and C elements using Lagrange equations

%% Symbols and functions

clear all, clc

syms t L C R q(t) u0(t) lambda1(t) lambda2(t) lambda3(t)

%% Magnetic co-energy, electric energy and dissipation function

cWm = 1/2*L*diff(q(t),t)^2 

We = 1/2*1/C*q(t)^2

D = 1/2*R*diff(q(t),t)^2

%% Lagrangian

Lf = cWm - We

%% Lagrange equation

Leq = diff(diff(Lf,diff(q(t),t)),t) - diff(Lf,q(t)) == -diff(D,diff(q(t),t)) + u0(t)

pretty(Leq)

%% Electric co-energy, magnetic energy and co-dissipation function

cWe = 1/2*C*diff(lambda3(t),t)^2

Wm = 1/2*1/L*(lambda2-lambda3)^2

cD = 1/2*1/R*(diff(lambda1(t),t)-diff(lambda2(t),t))^2

%% Co-Lagrangian

cLf = cWe-Wm

%% Lagrange equations
cLeq1 = diff(diff(cLf,diff(lambda2(t),t)),t) - diff(cLf,lambda2(t)) == -diff(cD,diff(lambda2(t),t))

cLeq2 = diff(diff(cLf,diff(lambda3(t),t)),t) - diff(cLf,lambda3(t)) == -diff(cD,diff(lambda3(t),t))

%%
% Rename the d/dt lambda1(t) as just u0(t)

cLeq1 = subs(cLeq1,diff(lambda1(t),t),u0(t))

%%
% Co-Lagrange equations are
cLeqA = [cLeq1; cLeq2]

%% 
% Alternatively after (manually) adding the first Lagrange equation (before renaming) to the second 
% and integrating it we get

cLeq2 = -(lambda1-lambda2)/R + C*diff(lambda3(t),t) == 0

cLeqB = [cLeq1; cLeq2]

%%
% But note that the last equation contains lambda1(t), which is essentially
% an intergral of the input voltage u0(t).

%% Reduce to first-order equations

[eqs,vars] = reduceDifferentialOrder(cLeqA,[lambda2(t),lambda3(t)])

%% Find explicit first-order differential equations

[M,F] = massMatrixForm(eqs,vars)
f = M\F


%% State-space models

clear all, clc

R = 1
L = 2
C = 3

%% From Lagrange equation (charge formalism)
 
G0 = tf(1,[L R 1/C])

%%
pole(G0)


%% From co-Lagrange equation (flux formalism)

A1 = [-R/L, R/L, 0; 0, 0, 1; 1/(L*C), -1/(L*C), 0]
B1 = [1; 0; 0]
C1 = [1/L, -1/L, 0]
D1 = 0

G1 = ss(A1,B1,C1,D1);
tf(G1)

%%

pole(G1)
tzero(G1)

%%

minG1 = minreal(G1);
tf(minG1)

%% From the alternative Lagrange equation 
% obtained by adding the first Lagrange equation to the second and
% integrating and adding an integrator

A2 = [-R/L, R/L, 0; -1/(R*C), 0, 1/(R*C);0, 0, 0]
B2 = [1; 0; 1]
C2 = [1/L, -1/L, 0]
D2 = 0

G2 = ss(A2,B2,C2,D2);
tf(G2)

%%

pole(G2)
tzero(G2)

%%

minG2 = minreal(G2);
tf(minG2)

%% From power bond graph 
% Not derived in this code

A3 = [-R/L, -1/C; 1/L, 0]
B3 = [1; 0]
C3 = [0, 1]
D3 = 0

G3 = ss(A3,B3,C3,D3);
tf(G3)

%%

pole(G3)
tzero(G3)
