\documentclass[a4paper,10pt]{article}

\usepackage[czech]{babel}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}

\usepackage{fullpage}

\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}

\usepackage{hyperref}

\usepackage{derivative}

\usepackage{graphicx}


\begin{document}

\section*{Modelování elektromagnetu Lagrangeovými rovnicemi}

Uvažujte elektromagnet načrtnutý na obrázku. Část o hmotnosti $M$ pohybující se translačně v jednom směru (na obrázku vertikálně) je přitahována v důsledku proudu procházejícího vodičem navinutým v $N$ závitech na magneticky měkkém materiálu. Oproti této přitažlivé síle působí pružina a tlumič.

\begin{figure}[ht]
 \centering
 \includegraphics[scale=0.4]{obrazky/schema_woodson.png}
 % schema_woodson.png: 632x690 px, 96dpi, 16.72x18.25 cm, bb=0 0 474 517
 \caption{Elektromagnet (z Woodson a Melcher (1968), strana 85)}
 \label{fig:schema_woodson}
\end{figure}

Lze ukázat, že za předpokladu magnetické měkkosti materiálu, na kterém je vodič navinut, a za předpokladu velmi malých (ve srovnání s ostatními rozměry) mezer $g$ a $x(t)$ je závislost indukčnosti vinutí na mezeře $x$ dostatečně přesně popsána\footnote{Woodson, Herbert H., and James R. Melcher. Electromechanical Dynamics, vol.1. New York, NY: John Wiley and Sons, Inc., 1968. Dostupné zdarma online na \href{https://ocw.mit.edu/resources/res-6-003-electromechanical-dynamics-spring-2009/}{Herbert Woodson, and James Melcher. RES.6-003 Electromechanical Dynamics. Spring 2009}. Massachusetts Institute of Technology: MIT OpenCourseWare, https://ocw.mit.edu. License: Creative Commons BY-NC-SA.} pomocí

\begin{equation*}
 L(x) = \frac{2wd\mu_0 N^2}{g+x}
\end{equation*}

Modelujte odezvu pozice $x$ pohyblivé části solenoidu na přiložené napětí $u$ a využijte k tomu metodu Lagrangeových rovnic.

\subsection*{Řešení}
Zobecněné souřadnice musí popisovat jak elektrickou tak i mechanickou část systému. Pro mechanickou část je přirozeným kandidátem pozice $x$ pohyblivé části (alternativně délka $l$ pružiny při natažení z rovnovážného stavu). Pro elektrickou část volíme nábojový formalismus a tedy přirozeným kandidátem je náboj $q$ coby integrál proudu protékajícího vinutím. Tedy

\begin{equation}
 \boldsymbol q = 
 \begin{bmatrix}
  q \\ x
 \end{bmatrix}
\end{equation}

Veškerá kinetická energie je akumulována v translačním pohybu jediné pohyblivé části. Související kinetická koenergie je

\begin{equation*}
 \mathcal{T}^* = \frac{1}{2}M\dot x^2.
\end{equation*}

Veškerá magnetická energie je akumulována v proudu vinutím. Související magnetická koenergie je

\begin{equation*}
\mathcal{W}_m^*(\dot q) = \frac{1}{2}L\dot q^2.  
\end{equation*}

Veškerá potenciální energie je akumulována v pružině roztažené z nominálního stavu. Jestliže při nulovém protékajícím proudu je protažení pružiny $l=0$ a $x=x_0$, pak po ustálení při nenulovém procházejícím proudu bude délka pružiny $l=x_0-x$. 

\begin{equation*}
 \mathcal{V}(x) = \frac{1}{2}K(x_0-x)^2.
\end{equation*}

Žádná elektrická energie v systému akumulována není (žádný kapacitor v elektrické části). Tedy 

\begin{equation*}
 \mathcal{W}_e(q) = 0.
\end{equation*}

Lagrangeova funkce je

\begin{align*}
 \mathcal{L}(\dot x, \dot q, x) &= \mathcal{T}^*(\dot x) + \mathcal{W}_m^*(\dot q) - \mathcal{V}(x)\\
 &= \frac{1}{2}M\dot x^2 + \frac{1}{2}L(x)\dot q^2 - \frac{1}{2}K(x_0-x)^2. 
\end{align*}

A ještě disipace energie. Zde k ní dochází jak v odporu vinutí, tak v tlumiči, který modeluje jak nezamýšlené třecí ztráty, tak případně i cíleně přidaný tlumič. Disipativní funkce je dána

\begin{equation*}
 \mathcal{D}(\dot x, \dot q) = \frac{1}{2}R\dot q^2 + \frac{1}{2}B\dot x^2.
\end{equation*}

Výpočet jednotlivých členů pro Lagrangeovy rovnice níže provedeme ručně a rozepíšeme po jednotlivých krocích:

\begin{align*}
 \pdv{\mathcal{L}}{\dot q} &= L(x)\dot q, \qquad 
 \odv*{\pdv{\mathcal{L}}{\dot q}}{t} = \odv{L(x)}{t} \dot q + L(x)\ddot q = \pdv{L(x)}{x} \dot x \dot q + L(x)\ddot q,\\
 \pdv{\mathcal{L}}{\dot x} &= M\dot x, \qquad 
 \odv*{\pdv{\mathcal{L}}{\dot x}}{t} = M\ddot x,\\
 \pdv{\mathcal{L}}{q} &= 0,\\
 \pdv{\mathcal{L}}{x} &= \frac{1}{2}\dot q^2\odv{L(x)}{x} - K(x_0-x),\\
 \pdv{\mathcal{D}}{\dot q} &= R\dot q, \qquad \pdv{\mathcal{D}}{\dot x} = B\dot x.
\end{align*}

Dosazením do dvou Lagrangeových rovnic

\begin{align*}
    \odv*{\pdv{\mathcal L}{\dot q}}{t} - \pdv{\mathcal L}{q} &= -\pdv{D}{\dot q} + u_s,\\
    \odv*{\pdv{\mathcal L}{\dot x}}{t} - \pdv{\mathcal L}{x} &= -\pdv{D}{\dot x}
\end{align*}

dostaneme

\begin{align*}
    \pdv{L(x)}{x} \dot x \dot q + L(x)\ddot q &= - R\dot q + u_s,\\
 M\ddot x - \frac{1}{2}\dot q^2\pdv{L(x)}{x} + K(x_0-x) &= -B\dot x.
\end{align*}

Snad ještě i vypočítat onu derivaci indukčnosti podle posunutí

\begin{equation*}
 \pdv{L(x)}{x} = \pdv*{\frac{2wd\mu_0 N^2}{g+x}}{x} = -\frac{2wd\mu_0 N^2}{(g+x)^2}.
\end{equation*}

Poznámka: takto odvozený model nevylučuje, že by mezera $x$ mohla být i záporná, což je samozřejmě fyzikálně nesmysl. Abychom to napravili, bylo by zapotřebí do modelu zakomponovat i omezení typu nerovnost.

\end{document}
