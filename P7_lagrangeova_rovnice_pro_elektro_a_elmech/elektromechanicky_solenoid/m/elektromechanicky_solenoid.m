%% Elektromechanický solenoid
%

%%
%
clear all, clc

%%
%
syms x(t) x0 g q(t) K B M R w d mu0 N u0

%%
%
L0 = w*d*mu0*N^2/g

L = L0/(1+x(t)/g)


%% Energie a koenergie

cWm = 1/2*L*diff(q(t),t)^2
We = 0

cT = 1/2*M*diff(x(t),t)^2
V = 1/2*K*(x0-x(t))^2

%% Lagrangeova funkce

Lfun = cWm + cT - We - V

%% Disipativní funkce

Dfun = 1/2*B*diff(x(t),t)^2 + 1/2*R*diff(q(t),t)^2

%% Lagrangeovy rovnice

Leq1 = diff(diff(Lfun,diff(q(t),t)),t) - diff(Lfun,q(t)) == -diff(Dfun,diff(q(t),t)) + u0
Leq2 = diff(diff(Lfun,diff(x(t),t)),t) - diff(Lfun,x(t)) == -diff(Dfun,diff(x(t),t))

%% Literatura
%
% Woodson, Herbert H., a James R. Melcher. Electromechanical Dynamics, Part
% 1: Discrete Systems. 1. vyd. Wiley, 1968. Strana 85.
%
% Také
%
% Preumont, Andrew. Mechatronics: Dynamics of Electromechanical and Piezoelectric Systems. Dordrecht: Springer, 2006. Strana 77. 