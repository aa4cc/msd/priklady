%% Inverted pendulum with vertically vibrating pivot

%%
% A textbook-style inverted pendulum (a massless rod and a bob of mass m 
% attached at the end) with the added feature that the (massless) pivot point 
% can move vertically.

%%
% <<inverted_pendulum_vibrating_pivot.png>>

%%
% See <https://youtu.be/5oGYCxkgnHQ video on Youtube>.

%% 
% Two degrees of freedom, hence two generalized variables:
%
% * the vertical position $y$ of the pivot point, 
% * the angular deviation $\theta$ of the pendulum rod from the upright orientation.

clear all, close all, clc

syms m g r y(t) theta(t) F(t)  

%%
% Position of the bob in the cartesian coordinate system (x horizontal, y vertical)

xm = sin(theta(t))*r
ym = y(t) + cos(theta(t))*r

%%
% The components of the velocity vector

vmx = diff(xm,t)
vmy = diff(ym,t)

%% Kinetic energy

T = 1/2*m*(vmx^2 + vmy^2)

%% Potential energy

V = m*g*(y(t)+r*cos(theta(t)))

%% Lagrangian

L = T-V

%%

pretty(L)

%% Lagrange equations
% Just the left-hand side (note that we subtract the general force that is
% typically written on the right-hand side)

Leq1 = diff(diff(L,diff(y(t),t)),t) - diff(L,y(t)) == F(t)
Leq2 = diff(diff(L,diff(theta(t),t)),t) - diff(L,theta(t)) == 0

%%

pretty(Leq1)

%%

pretty(Leq2)

%%

Leq = [Leq1;Leq2]
%% State equations

[eqs,vars] = reduceDifferentialOrder(Leq,[y(t),theta(t)])
[M,F] = massMatrixForm(eqs,vars)
f = M\F

%% Simulation

syms t

m = 1;
r = 1;
g = 9.81;

w = 500;
Fmax = 10000;

F(t) = m*g + Fmax*cos(w*t);

f = subs(f)  % replaces the symbolic parameters with their values in the workspace

odefun = odeFunction(f,vars);

%% Setting the simulation parameters and calling an ODE solver
tspan = [0 5];

y0 = 0; theta0 = 0.2; v0 = 0.0; omega0 = 0;
initConditions = [y0;theta0;v0;omega0];
opts = odeset('RelTol',1e-5,'AbsTol',1e-5);

[t,x] = ode23s(odefun, tspan, initConditions);

%% Plotting the solutions
figure(1)
subplot(3,1,1)
plot(t,F(t))
ylabel('F(t) [N]')
subplot(3,1,2)
plot(t,x(:,1))
ylabel('y(t) [m]')
subplot(3,1,3)
plot(t,x(:,2))
xlabel('t [s]')
ylabel('\theta(t) [rad]')

xp = zeros(length(x(:,1)),1);
yp = x(:,1);
xb = r*sin(x(:,2));
yb = yp + r*cos(x(:,2));

figure(2)
subplot(2,1,1)
plot(t,xb)
ylabel('x_b [m]')
subplot(2,1,2)
plot(t,yb)
ylabel('y_b [m]')
xlabel('t [s]')

figure(3)
plot(xb,yb)
xlabel('x [m]')
ylabel('y [m]')