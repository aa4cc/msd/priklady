%% Variable length pendulum

%%
% Consider the standard pendulum on the picture below. All the mass is
% concentrated in the bob (the rope is considered massless). However, the
% length of the rope can be changed.

%%
% <<pendulum_with_variable_length.png>>

%% 

clear all, close all, clc

%% Symbolic variables and functions

syms m g l(t) phi(t) 

%% Coordinates of the pendulum
% In order to characterize the configuration of the systems fully, we need
% to specify both $l(t)$ and $\phi(t)$. However, it appears advantageous to transform 
% these into the cartesian coordinates because then the computation of the kinetic and
% potential energies will be straightforward.

x(t) = l(t)*sin(phi(t));
y(t) = l(t)*cos(phi(t));

%% Computing the velocities

vx(t) = diff(x(t),t)
vy(t) = diff(y(t),t)

%% Kinetic energy

T = 1/2 * m * (vx(t)^2 + vy(t)^2);
T = simplify(T)

%%

pretty(T)

%% Alternative computation of kinetic energy
% I argued above that we needed the cartesian coordinates in order to be able 
% to compute the energies. Note however, that here the derivative of the length
% gives the radial component of the velocity and the derivative of the
% angle after multiplication by the length of the pendulum gives the tangential
% component of the velocity. These two are also orthogonal and we can use
% them directly for computing the square of the total velocity needed in
% the computation of the kinetic energy.

vr(t) = diff(l(t),t)
vt(t) = l(t)*diff(phi(t),t)

T2 = 1/2 * m * (vr(t)^2 + vt(t)^2)

%%

pretty(T2)

%% Potential energy

V = - m*g*y(t)

%% Lagrange function 

L = T-V

%% Lagrange equation

Leq = diff(diff(L,diff(phi(t),t)),t) - diff(L,phi(t)) == 0


%%
% But we do not even need to bother and compute the partial derivatives by
% ourselves. Matlab has a useful function <matlab:doc('functionalDerivative') functionalDerivative> which for a
% given Lagrangian gives the left-hand side of the Lagrange equation (mind
% the minus sign).

Leqb = functionalDerivative(L,phi(t)) == 0

%% State equation
% We now need to extract the highest, that is, the second derivative of phi
% and then we are immediately ready to write down the state equation. It
% appears that even solving the equation for a function is not possible in Symbolic Math Toolbox.
% Once again a clumsy substition can find its use here. But there is a
% neater way using a higher level function, see the next section. We start
% with the painful substition, just for the completeness of the exposition:

syms ddotphi  % my choice for the latex-like name of d^2/dt^2 phi(t)

Leqc = subs(Leq,diff(phi(t),t,t),ddotphi)

%%
% The second derivative of the angle is given by

ddotphi_sol = solve(Leqc,ddotphi)

%% 
% We now introduce a new variable - the angular velocity $\omega(t)$ 

syms omega(t)

%%
% and the right hand side of one of the two state equations is

f2 = subs(ddotphi_sol,diff(phi(t),t),omega(t));

%%
% and the right hand side of the other state equation is

f1 = omega(t);

%% 
% Combining the two into a single vector function we get

f = [f1;f2]

%% Alternative procedure for converting Lagrange equation to state equations using some Symbolic Tbx functionalities
% There are two useful functions in Symbolic Toolbox, namely
% <matlab:doc('reduceDifferentialOrder') reduceDifferentialOrder>  and <matlab:doc('massMatrixForm') massMatrixForm>.

%% 
% First, we reduce the original second-order ODE to two first-order ODEs:

[eqs,vars] = reduceDifferentialOrder(Leq,phi(t))

%%
% Second, we convert this set of equations to the format M y' = ... :

[M,F] = massMatrixForm(eqs,vars)

%%
% And finally we invert the M matrix to obtain standard state equations:

fs = M\F

%% Converting the symbolic functions to numerical 
% Whichever way we obtained the matlab functions for the right hand side of
% the state equations, we need to solve them numerically. In order to do
% that, we have to convert the format from a symbolic to numerical. For
% that <matlab:doc('odeFunction') odeFunction> is useful. We must also
% evaluate the definition of the problem with some concrete real parameters
% and the prescription for l(t). 

syms t

l(t) = 1 + 0.2*sin(10*t);
% l(t) = 1;  % This is here just for checking the model in case of constant lenght of the pendulum
m = 1;
g = 9.8;

f = subs(f)  % replaces the symbolic parameters with their values in the workspace

odefun = odeFunction(f,[phi(t) omega(t)]);

%% Setting the simulation parameters and calling an ODE solver
tspan = [0 20];
initConditions = [pi/2; 0];

[t,x] = ode45(odefun, tspan, initConditions);

%% Plotting the solutions
figure(1)
subplot(3,1,1)
plot(t,l(t))
ylabel('l(t) [m]')
subplot(3,1,2)
plot(t,x(:,1))
ylabel('\phi(t) [rad]')
subplot(3,1,3)
plot(t,x(:,2))
xlabel('t [s]')
ylabel('\omega(t) [rad/s]')

%% 
% For a motivation for this modeling, see the paper B. Piccoli a J. Kulkarni. Pumping a swing by standing and squatting: 
% do children pump time optimally?, IEEE Control Systems, vol.25, no.4,
% pages 48-56, August 2005. <https://doi.org/10.1109/MCS.2005.1499390 DOI:
% 10.1109/MCS.2005.1499390>.
