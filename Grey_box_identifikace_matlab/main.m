%% Priklad grey-box identifikace parametrů (ne)lineárního modelu
% Priklad je inspirovany MATLAB prikladem: https://www.mathworks.com/help/ident/ref/nlgreyest.html
% Postup je stejny pro linearni i nelinearni systemy (modely)

%% DC motor

% Postup si ukazeme na linearnim modelu DC motoru: rovnice (8) a (9) zde:
% https://ctms.engin.umich.edu/CTMS/index.php?example=MotorSpeed&section=SystemModeling

% Uvazovany model je spojity system 2. radu (dva stavy), s jednim vstupem a jednim vystupem.
% Predpokladme, ze zname rovnice, ale jednotlive paramety nezname, pripadne
% zname jenom nektere z nich a nebo alespon jejich odhad.
% Mame ale namerena data ze systemu (vstup a vystup). Vstup muze byt primo
% datova rada, ale i analyticky zadany signal s definovanou vzorkovaci
% periodou.

Ts = 0.01; % Vzorkovaci frekvence
Tend = 5; % Delka

time = (0:Ts:Tend)'; % Sloupcovy vektor casu
idf_input_f = @(t) sin(10*t); % Anonymni funkce definujici vstup
[idf_measurement, idf_input_dat] = measure_data_dc_motor(idf_input_f, time);  % Merene vystupy a vstup (datove rady)

figure;
subplot(2,1,1)
hold on; grid on;
plot(time, idf_measurement);
xlabel("Time [s]");
ylabel("Motor angle [rad]");
subplot(2,1,2)
hold on; grid on;
plot(time, idf_input_dat);
xlabel("Time [s]");
ylabel("Input voltage [V]");

%% Identifikace
% Z identifikacnich dat je potreba udelat objekt "iddata".
% Vstupy a vystupy musi byt nasbirany se stejnou vzorkovaci periodou a mit
% stejnou (casovou) delku. Data musi byt pouze z jedne trajektorie.
data_idf = iddata(idf_measurement,idf_input_dat,Ts); % Argumenty: (mereni, vstupy, vzorkovaci perioda signalu).


% === Definice modelu, ktery chceme identifikovat ===
order = [1, 1, 2]; % Definuje pocet: [#vystupu, #vstupu, #stavu]

% = Parametry =
% Prvotni odhad parametru
J_guess = 0.1; % Moment setrvacnosti hridele
b_guess = 0.5; % Koeficient treni
R_guess = 1;   % Odpor vinuti
L_guess = 1;   % Indukcnost
% Parametry, ktere zname presne
K_exact = 0.01; % EMF konstanta motoru

parameters = {J_guess, b_guess, K_exact, R_guess, L_guess}; % Parametry systemu


% === Tvorba objektu, ktery reprezentuje (ne)linearni system
% Nejprve je nutne ziskat model systemu, ktery chceme identifikovat.
% Model muzem byt spojity i diskretni.
% Rovnice definujici system je nutne zapsat do Matlab funkce.
f_name = 'dc_motor_idf_func'; % vice informaci je v souboru (dc_motor_idf_func.m)

Ts_sys = 0; % Vzorkovaci perioda systemu (! nikoliv identifikacnich dat), ktery chceme identifikovat. 0 znaci spojity system
x0_idf = [idf_measurement(1); 0]; % Pocatecni stav systemu. Natoceni hridele zname, druhy stav (proud) nezname, ale lze predpokladat, ze byl nulovy.
nonlinear_model = idnlgrey(f_name,order,parameters,x0_idf,Ts_sys);

% Specifikace identifikace
setpar(nonlinear_model,'Minimum',{eps, 0, eps, eps, eps}); % Minimalni hodnoty parametru. Dulezite zejmena pro osetreni neplatnich matematickych operaci (deleni nulou)
setpar(nonlinear_model,'Fixed',{false, false, true, false, false}); % Hodnotu 3. parametru (K) zname presne, proto je fixni


opt = nlgreyestOptions;
opt.SearchOptions.MaxIterations = 20; % Maximalni pocet iteraci, zvyseni tohoto parametru muze zlepsit identifikaci za cenu delsiho behu
opt.Display = "Full"; % Plny vypis
% opt.SearchMethod = 'gn'; % Metoda optimalizace. Muze vyrazne ovlivnit vysledek

% === Spusteni identifikace ====
nonlinear_model = nlgreyest(data_idf,nonlinear_model, opt);

% Extrahovani identifikovanych parametru
J_idf = nonlinear_model.Parameters(1).Value;
b_idf = nonlinear_model.Parameters(2).Value;
K_idf = nonlinear_model.Parameters(3).Value; % Stejna hodnota jako K_exact
R_idf = nonlinear_model.Parameters(4).Value;
L_idf = nonlinear_model.Parameters(5).Value;

%% Porovnani identifikovaneho modelu s realnymi (verifikacnimi) daty

verif_input_f = @(t) sin(10*t)' + 0.5*sin(15*t)'; % Definice vstupu jako anonymni funkce
verif_measurement = measure_data_dc_motor(verif_input_f, time); % funkce "measure_data_dc_motor" ma jako argument datovou radu  

x0 = [0;0];
tspan = [0 5];
[T,Y] = ode45(@(t,x) dc_motor_func(t,x, verif_input_f, J_idf, b_idf, K_exact, R_idf, L_idf),  tspan, x0);

figure;
hold on; grid on;
plot(time, verif_measurement, 'LineWidth',1.5);
plot(T, Y(:, 1), 'LineStyle','--', 'LineWidth',1.5);
legend("Verification data", "Simulation data")
xlabel("Time [s]");
ylabel("Motor angle [rad]");

% Pozn.: Porovnate-li realne parametry (ve funkci "measure_data_dc_motor")
% a identifikovane paramety, lze videt, ze i presto, ze identifikace i
% verifikace jsou vcelku presne, parametry nejsou stejne. V systemu totiz
% vystupuji podily jednotlivych parametru, a tedy nektere parametry je mozne "reskalovat" (rescale)





