function [output, in_signal] = measure_data_dc_motor(in_func, t)
% Tato funkce nahrazuje mereni dat na realnem systemu

% Parametry systemu, ktere nezname. Zde slouzi pouze ke generovani dat
J = 0.01;
b = 0.1;
K = 0.01;
R = 1;
L = 0.5;

% Tvorba linearniho systemu
A = [-b/J   K/J
    -K/L   -R/L];
B = [0
    1/L];
C = [1   0];
D = 0;
motor_ss = ss(A,B,C,D);


in_signal = in_func(t); % Evaluace funkce na casovem vektoru pro ziskani datove rady.
output = lsim(motor_ss,in_signal,t);


end

