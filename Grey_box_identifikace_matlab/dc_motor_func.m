function dxdt = dc_motor_func(t,x,u, J,b,K,R,L)

% Rozdil oproti funkci: "dc_motor_idf_func" 
% u(t) je funkce času, nikoliv časová řada

dxdt = [ -b/J*x(1) + K/J*x(2);
         -K/L*x(1) - R/L*x(2) + 1/L*u(t)];

end

