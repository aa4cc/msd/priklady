function [dxdt,y] = dc_motor_idf_func(t,x,u, J,b,K,R,L, varargin)
%DC_MOTOR_IDF_FUNC 
%   Pro tuto funkci plati:
%       - Musi mit dva vystupy:
%           - dxdt: vektor leve strany stavoveho modelu systemu
%           - y: vektor vsech vystupu (outputs) systemu 
%       - Poradi vstupnich argumentu:
%           - t: cas 
%           - x: stavovy vektor
%           - u: vstup do systemu
%           - Postupne vsechny zname i nezname parametry systemu
%           - Na konci parametr "varargin"

% Zde muze byt i nelinearni model:
dxdt = [ -b/J*x(1) + K/J*x(2);
         -K/L*x(1) - R/L*x(2) + 1/L*u];

y = x(1);

end

