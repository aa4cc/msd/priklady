%% Dopředná a zpětná Eulerova metoda a vliv délky integračního kroku na chybu

%% Symbolické řešení obyčejné diferenciální rovnice
% Uvažujme rovnici $\dot x(t) = x(t)-2tx(t)^2/(1+t)$.

clear all

%% 
% S využitím toolboxu pro symbolické výpočty můžeme najít obecné řešení

dsolve('Dx=(x-2*t*x^2)/(1+t)')

%%
% Coby diferenciální rovnice prvního řádu je toto řešení parametrizováno 
% jedinou konstantou. Ta má význam počáteční hodnoty řešení. Pro různé její hodnoty vykreslíme odpovídající konkrétní průběhy. 

t0 = 0; tf = 1; x0 = 5; 
t = linspace(t0,tf,1000);
C = logspace(-1,1,30);
x = zeros(length(C),length(t));

for ii = 1:length(C)
    x(ii,:) = (t + 1)./(t.^2 + C(ii));
end

plot(t,x)
xlabel('t [s]')
ylabel('x(t)')

%% Numerické řešení s využitím dopředné Eulerovy metody pro zvolenou délku diskretizačního (integračního) kroku
% Než začneme implementovat numerický algoritmus, zadefinujeme si tady
% funkci vyjadřující přesné řešení získané analyticky. Budeme s ním
% srovnávat výstupy numerických algoritmů.

xtrue = @(t) (1+t)/(t^2+1/5);  % Skutecne reseni spocitane analyticky

%%
% Zvolíme (svévolně) délku kroku 
h = 0.2;

%%
% Zafixujeme i konkrétní počáteční hodnotu 
x = x0;

%%
% A nyni uz je mozne inicializovat samotny iterativni algoritmus
t = t0;
ix = 1;

%%
% a začít iterace. V každé iteraci nejen že spočítáme dopředný Eulerův
% krok, ale i srovnáme získanou aproximaci s přesným řešením. Toto
% samozřejmě v reálné situaci nebude k dispozici, ale teď pro účely výuky
% si ten luxus známosti skutečného řešení dopřát můžeme.

while t <= (tf-h)+h/1000
    dxdt = (x(ix)-2*t*x(ix)^2)/(1+t);
    x(ix+1) = x(ix)+h*dxdt;
    t = t+h;
    error(ix) = x(ix)-xtrue(t);
    str = sprintf('ix=%d \t tk=%6.4f \t xk=%6.4f \t xk_true=%6.4f \t error=%6.4f',ix,t,x(ix),xtrue(t),error(ix));
    disp(str)
    ix = ix+1;
end

%%
% A nakonec vykreslíme výsledky numerického řešení oproti té rodině analytických řešení

hold on,
hf1 = plot(t0:h:tf,x(1:end),'o-');
set(hf1,'LineWidth',2)
hold off

%%
% Zjevně je to numerické řešení zejména zezačátku docela nepřesné.

%% 
% Spočítat si můžeme také globální (=na konci intervalu) chybu. 
% Nejdříve si ale připravím "kontejner" na výsledky těchto analýz pro tento a dalších čtyři pokusy 

global_error = zeros(5,1);

global_error(1)= error(end);

%%
% Připomenu, že jelikož z teoretické analýzy víme, že globální chyba u dopředné 
% Eulerovy metody závisí na délce kroku lineárně. Proto si spočítíme ten
% koeficient lineární závislosti
global_error(1)/h

%%
% i relativní (=vztaženou k hodnotě řešení) globální chybu.
relative_global_error(1) = global_error(1)/x(end);

%%
% print -depsc2 ../../lectures/figures/ex_FE_h02.eps
% !epstopdf ../../lectures/figures/ex_FE_h02.eps

%% Numerické řešení pro jinou (kratší) délku kroku
% Vzhledem ke zjevné neužitečnosti předchozího řešení se můžeme pokusit
% situaci zlepšit zmenšením integračního kroku. Ten zmenšíme na polovinu:

h = 0.1;

%%
% Zafixujeme i konkrétní počáteční hodnotu 
x = x0;

%%
% A nyni uz je mozne inicializovat samotny iterativni algoritmus
t = t0;
ix = 1;

%%
% a začít iterace. V každé iteraci nejen že spočítáme dopředný Eulerův
% krok, ale i srovnáme získanou aproximaci s přesným řešením. Toto
% samozřejmě v reálné situaci nebude k dispozici, ale teď pro účely výuky
% si ten luxus známosti skutečného řešení dopřát můžeme.

while t <= (tf-h)+h/1000
    dxdt = (x(ix)-2*t*x(ix)^2)/(1+t);
    x(ix+1) = x(ix)+h*dxdt;
    t = t+h;
    error(ix) = x(ix)-xtrue(t);
    str = sprintf('ix=%d \t tk=%6.4f \t xk=%6.4f \t xk_true=%6.4f \t error=%6.4f',ix,t,x(ix),xtrue(t),error(ix));
    disp(str)
    ix = ix+1;
end

%%
% A nakonec vykreslíme výsledky numerického řešení oproti té rodině analytických řešení

hold on,
delete(hf1)
hf2 = plot(t0:h:tf,x(1:end),'o-');
set(hf2,'LineWidth',2)
hold off

%%
% Zjevně je to numerické řešení výrazně blíže k tomu správnému.

%% 
% Spočítat si můžeme také globální (=na konci intervalu) chybu 

global_error(2) = error(end);

%%
% i její závislost na délce integračního kroku
global_error(2)/h

%%
% i relativní (=vztaženou k hodnotě řešení) globální chybu pro pozdější
% analýzu závislosti velikosti této chyby na délce integračního kroku.
relative_global_error(2) = global_error(2)/x(end);

%%
% print -depsc2 ../../lectures/figures/ex_FE_h01.eps
% !epstopdf ../../lectures/figures/ex_FE_h01.eps

%% Numerické řešení pro ještě kratší integrační krok

h = 0.05;
%%
% Zafixujeme i konkrétní počáteční hodnotu 
x = x0;

%%
% A nyni uz je mozne inicializovat samotny iterativni algoritmus
t = t0;
ix = 1;

%%
% a začít iterace. V každé iteraci nejen že spočítáme dopředný Eulerův
% krok, ale i srovnáme získanou aproximaci s přesným řešením. Toto
% samozřejmě v reálné situaci nebude k dispozici, ale teď pro účely výuky
% si ten luxus známosti skutečného řešení dopřát můžeme.

while t <= (tf-h)+h/1000
    dxdt = (x(ix)-2*t*x(ix)^2)/(1+t);
    x(ix+1) = x(ix)+h*dxdt;
    t = t+h;
    error(ix) = x(ix)-xtrue(t);
    str = sprintf('ix=%d \t tk=%6.4f \t xk=%6.4f \t xk_true=%6.4f \t error=%6.4f',ix,t,x(ix),xtrue(t),error(ix));
    disp(str)
    ix = ix+1;
end

%%
% A nakonec vykreslíme výsledky numerického řešení oproti té rodině analytických řešení

hold on,
delete(hf2)
hf3 = plot(t0:h:tf,x(1:end),'o-');
set(hf3,'LineWidth',2)
hold off

%%
% Zjevně pokračuje ten trend přibližování numerického řešení k tomu
% skutečnému (získanému symbolicky).

%% 
% Spočítat si můžeme také globální (=na konci intervalu) chybu 

global_error(3) = error(end);
global_error(3)/h

%%
% i relativní (=vztaženou k hodnotě řešení) globální chybu pro pozdější
% analýzu závislosti velikosti této chyby na délce integračního kroku.
relative_global_error(3) = global_error(3)/x(end);


% print -depsc2 ../../lectures/figures/ex_FE_h005.eps
% !epstopdf ../../lectures/figures/ex_FE_h005.eps

%% A ještě kratší

h = 0.025;

%%
% Zafixujeme i konkrétní počáteční hodnotu 
x = x0;

%%
% A nyni uz je mozne inicializovat samotny iterativni algoritmus
t = t0;
ix = 1;

%%
% a začít iterace. V každé iteraci nejen že spočítáme dopředný Eulerův
% krok, ale i srovnáme získanou aproximaci s přesným řešením. Toto
% samozřejmě v reálné situaci nebude k dispozici, ale teď pro účely výuky
% si ten luxus známosti skutečného řešení dopřát můžeme.

while t <= (tf-h)+h/1000
    dxdt = (x(ix)-2*t*x(ix)^2)/(1+t);
    x(ix+1) = x(ix)+h*dxdt;
    t = t+h;
    error(ix) = x(ix)-xtrue(t);
    str = sprintf('ix=%d \t tk=%6.4f \t xk=%6.4f \t xk_true=%6.4f \t error=%6.4f',ix,t,x(ix),xtrue(t),error(ix));
    disp(str)
    ix = ix+1;
end

%%
% A nakonec vykreslíme výsledky numerického řešení oproti té rodině analytických řešení

hold on,
delete(hf3)
hf4 = plot(t0:h:tf,x(1:end),'o-');
set(hf4,'LineWidth',2)
hold off

%%
% Tady už na první pohled nelze to řešení odlišit od toho přesného (a to
% ani po zazoomování)

%% 
% Spočítat si můžeme také globální (=na konci intervalu) chybu 

global_error(4) = error(end);
global_error(4)/h

%%
% i relativní (=vztaženou k hodnotě řešení) globální chybu pro pozdější
% analýzu závislosti velikosti této chyby na délce integračního kroku.
relative_global_error(4) = global_error(4)/x(end);

%%
% print -depsc2 ../../lectures/figures/ex_FE_h0025.eps
% !epstopdf ../../lectures/figures/ex_FE_h0025.eps

%% A ještě kratší

h = 0.0125;

%%
% Zafixujeme i konkrétní počáteční hodnotu 
x = x0;

%%
% A nyni uz je mozne inicializovat samotny iterativni algoritmus
t = t0;
ix = 1;

%%
% a začít iterace. V každé iteraci nejen že spočítáme dopředný Eulerův
% krok, ale i srovnáme získanou aproximaci s přesným řešením. Toto
% samozřejmě v reálné situaci nebude k dispozici, ale teď pro účely výuky
% si ten luxus známosti skutečného řešení dopřát můžeme.

while t <= (tf-h)+h/1000
    dxdt = (x(ix)-2*t*x(ix)^2)/(1+t);
    x(ix+1) = x(ix)+h*dxdt;
    t = t+h;
    error(ix) = x(ix)-xtrue(t);
    str = sprintf('ix=%d \t tk=%6.4f \t xk=%6.4f \t xk_true=%6.4f \t error=%6.4f',ix,t,x(ix),xtrue(t),error(ix));
    disp(str)
    ix = ix+1;
end

%%
% A nakonec vykreslíme výsledky numerického řešení oproti té rodině analytických řešení

hold on,
delete(hf4)
hf5 = plot(t0:h:tf,x(1:end),'o-');
set(hf5,'LineWidth',2)
hold off

%%
% Stejně jako v předchozím případě ani tady už nelze jen tak vizuálně to
% řešení odlišit od toho analyticky spočítaného, a to ani po zazoomování.

%% 
% Spočítat si můžeme také globální (=na konci intervalu) chybu 

global_error(5) = error(end);
global_error(5)/h

%%
% i relativní (=vztaženou k hodnotě řešení) globální chybu pro pozdější
% analýzu závislosti velikosti této chyby na délce integračního kroku.
relative_global_error(5) = global_error(5)/x(end);

%%
% print -depsc2 ../../lectures/figures/ex_FE_h00125.eps
% !epstopdf ../../lectures/figures/ex_FE_h00125.eps

%% Analýza závislosti chyby na délce integračního kroku

h = [0.2 0.1 0.05 0.025 0.0125];
plot(h,abs(global_error),'o-','LineWidth',2)
xlabel('h [s]')
ylabel('|Global error|')

%% Zpětná Eulerova metoda
% ...


