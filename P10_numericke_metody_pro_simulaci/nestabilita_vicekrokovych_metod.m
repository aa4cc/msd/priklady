%% Nestabilita vícekrokových metod

%%
clear all, close all, clc

%%
% Uvažujme diferenciální rovnici $\dot x = -x,\quad x(0)=1$.

f = @(x) -x;
x0 = 1;

%% Analytické (přesné) řešení

xt = @(t) exp(-t)

%% Numerické řešení

h = 0.0001;
tf = 0.0023;        % pro koncový čas větší než nějakých 0.23 už je ta divergence obrovská

% Inicializace prvních dvou hodnot
x(1) = x0;
x(2) = exp(-h);

k=3;
t = (k-1)*h;

while t <= tf
    x(k) = -4*x(k-1) + 5*x(k-2) + h*(4*f(x(k-1)) + 2*f(x(k-2)));
    k = k+1;
    t = t+h;
end

%% Vykreslení

tt = 0:h:(k-2)*h;
plot(tt,x,'.')
xlabel('t')
ylabel('x(t)')
hold on

%%
% Nad to simulované řešení ještě i to přesné

fplot(xt,[0,tf])
hold off

%%

% print -depsc2 ../../figures/instability_multistep.eps
% !epstopdf ../../figures/instability_multistep.eps