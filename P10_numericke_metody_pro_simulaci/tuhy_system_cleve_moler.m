%% Tuhé diferenciální rovnice
% Postaveno na Cleve Moler: Stiff differential equations. Matlab Digest, 2003. Online https://www.mathworks.com/company/newsletters/articles/stiff-differential-equations.html

%% Zadání
%
% $$\dot x = x^2 - x^3,\quad x(0)=0.01.$$

%% Analytické řešení s využitím Lambertovy funkce

syms x(t)
deqn = diff(x,t) == x^2 - x^3
ic = x(0) == 1/100
xsol = dsolve(deqn,ic);
xsol = simplify(xsol);
pretty(xsol)
ezplot(xsol,0,200)

%% Numerické řešení s využitím ODE45

d = 0.01;
F = @(t,x) x^2 - x^3;
%opts = odeset('RelTol',1.e-4);
[tout,xout] = ode45(F,[0 2/d],d);
semilogy(tout(1:end-1),diff(tout),'.-')
ylabel('Delka kroku')
xlabel('t')

%%
% Počet integračních kroků

length(tout)

%%

plot(tout,xout,'.','MarkerSize',10)
ylabel('x(t)')
xlabel('t')

%% Numerické řešení s využitím solveru specializovaného na tuhé systémy

[tout,xout] = ode23s(F,[0 2/d],d);
semilogy(tout(1:end-1),diff(tout),'.-')
ylabel('Delka kroku')
xlabel('t')

%%
% Počet integračních kroků

length(tout)

%%

plot(tout,xout,'.','MarkerSize',10)
ylabel('x(t)')
xlabel('t')