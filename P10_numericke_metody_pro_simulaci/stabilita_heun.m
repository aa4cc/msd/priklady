h = 1;
I = eye(2,2);

figure(1)

xlabel('Real')
ylabel('Imaginary')

a = linspace(-4,2,100); % range of real values
b = linspace(0,5,100);    % range of imaginary values

rmax = zeros(length(a),length(b));

for ia = 1:length(a)
    for ib = 1:length(b)
        A = [a(ia) b(ib); -b(ib) a(ia)];
        %F = I + A*h + A^2*h^2/2;
        F = I + A*h;
        %F = inv(I - A*h);
        %F = I + A*h + 1/3*A^2*h + 1/3*h^2*A^2+1/24*h^3*A^3;
        rmax(ia,ib) = max(max(abs(eig(F))));  % spectral radius    
    end
end

[A,B] = meshgrid(a,b); 
surfc(A,B,rmax')
xlabel('Re(\lambda)')
ylabel('Im(\lambda)')
zlabel('\rho(F)')

figure(2)
contour(A,B,rmax',[1 1])
xlabel('Re(\lambda)')
ylabel('Im(\lambda)')
axis([a(1) a(end) b(1) b(end)])

