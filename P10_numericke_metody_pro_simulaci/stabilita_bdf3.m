% Code for plotting (absolute, A) stability domains for a few numerical integration methods
%
% Author: Zdenek Hurak
% Date: September 19, 2011.

h = 1;              % normalized length of the integration step 
I = eye(2,2);
O = zeros(2,2);

figure(1)

xlabel('Real'), ylabel('Imaginary')

a = linspace(-2,8,100); % range of real values
b = linspace(0,5,100);    % range of imaginary values

rmax = zeros(length(a),length(b));

for ia = 1:length(a)
    for ib = 1:length(b)
        A = [a(ia) b(ib); -b(ib) a(ia)];
        A6 = zeros(6,6); A6(5:6,5:6) = A;
        F = inv(eye(6,6)-6/11*A6*h)*[O, I, O; O, O, I; 2/11*I, -9/11*I, 18/11*I]; 
        rmax(ia,ib) = max(max(abs(eig(F))));  % spectral radius    
    end
end

[A,B] = meshgrid(a,b); 
surfc(A,B,rmax')
xlabel('Re(\lambda)')
ylabel('Im(\lambda)')
zlabel('\rho(F)')

%% Printing the figures
% print -depsc2 ../../figures/bdf3_stability_domain.eps
% !epstopdf ../../figures/bdf3_stability_domain.eps

%%
figure(2)
colormap(gray)
contourf(A,B,rmax',linspace(0,1,20))
%caxis([-10 1])
xlabel('Re(h\lambda)')
ylabel('Im(h\lambda)')
axis([a(1) a(end) b(1) b(end)])
grid on

%% Printing the figures
% print -depsc2 ../../figures/bdf3_stability_domain_contours.eps
% !epstopdf ../../figures/bdf3_stability_domain_contours.eps