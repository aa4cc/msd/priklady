%% Demonstrace proměnlivosti délky kroku

%% Otevření simulačního modelu
% Nejdříve pro jistotu

clear all, close all, clc

%%
% Otevření modelu ručně nebo příkazem

open promenna_delka_integracniho_kroku_sim

%% Simulace
% V nastavení Simulinku (Ctrl+E a záložka Data Import/Export) ale nutno zaškrtnout, 
% aby byl přímo do pracovního exportován vektor tout integračních časů. 

sim('promenna_delka_integracniho_kroku_sim')

%% 
% A nebo by to šlo přímo z příkazové řádky, ale pak bude výsledek simulace
% ve struktuře

% simout = sim('promenna_delka_integracniho_kroku_sim','SaveTime','on')
% tout = simout.tout;

%% Vykreslení délky kroku 
% Podle
% <href=https://blogs.mathworks.com/simulink/2012/06/04/the-most-useful-command-for-debugging-variable-step-solver-performance/ Guy Rouleau: THE Most Useful Command for Debugging Variable Step Solver Performance, 2012>.

semilogy(tout(1:end-1),diff(tout),'.-')

%%
length(tout)

%% Změna horní meze na integračního krok
% Výchozí hodnota při nastavení 'auto' je v tomto případě 0.2 (i jsme na to 
% dostali varovné hlášení). Nastavme něco výrazně delšího, aby ta hodnota nebyla nikdy 
% omezující, třebas 10. Ručně...
%
% ... a nebo z příkazové řádky

simout = sim('promenna_delka_integracniho_kroku_sim','SaveTime','on','MaxStep','10')
tout = simout.tout;

%%
% Pak opět

semilogy(tout(1:end-1),diff(tout),'.-')

%%
length(tout)