% Code for plotting (absolute, A) stability domains for a few numerical integration methods
%
% Author: Zdenek Hurak
% Date: September 19, 2011.

h = 1;              % normalized length of the integration step 
I = eye(2,2);

figure(1)

xlabel('Real'), ylabel('Imaginary')

a = linspace(-4,2,100); % range of real values
b = linspace(0,5,100);    % range of imaginary values

rmax = zeros(length(a),length(b));

for ia = 1:length(a)
    for ib = 1:length(b)
        A = [a(ia) b(ib); -b(ib) a(ia)];
        F = I + A*h + A^2*h^2/2;           % Heun
        %F = I + A*h;                       % Forward Euler
        F = inv(I - A*h);                   % Backward Euler
        F = I + A*h + 1/2*A^2*h^2 + 1/6*h^3*A^3+1/24*h^3*A^3; % RK4
        rmax(ia,ib) = max(max(abs(eig(F))));  % spectral radius    
    end
end

[A,B] = meshgrid(a,b); 
surfc(A,B,rmax')
xlabel('Re(\lambda)')
ylabel('Im(\lambda)')
zlabel('\rho(F)')

%% Printing the figures
%print -depsc2 ../figures/heun_stability_domain.eps
%!epstopdf ../figures/heun_stability_domain.eps
% 
% print -depsc2 ../figures/FE_stability_domain_contours.eps
% !epstopdf ../figures/FE_stability_domain_contours.eps

%print -depsc2 ../figures/BE_stability_domain_surfc.eps
%!epstopdf ../figures/BE_stability_domain_surfc.eps

% print -depsc2 ../figures/RK4_stability_domain_contours.eps
% !epstopdf ../figures/RK4_stability_domain_contours.eps

%%
figure(2)
colormap(gray)
contourf(A,B,rmax',linspace(0,1,20))
%caxis([-10 1])
xlabel('Re(h\lambda)')
ylabel('Im(h\lambda)')
axis([a(1) a(end) b(1) b(end)])
grid on

%% Printing the figures
%print -depsc2 ../figures/heun_stability_domain.eps
%!epstopdf ../figures/heun_stability_domain.eps
% 
% print -depsc2 ../figures/FE_stability_domain_contours.eps
% !epstopdf ../figures/FE_stability_domain_contours.eps

% print -depsc2 ../figures/BE_stability_domain_contours.eps
% !epstopdf ../figures/BE_stability_domain_contours.eps

% print -depsc2 ../figures/RK4_stability_domain_contours.eps
% !epstopdf ../figures/RK4_stability_domain_contours.eps
