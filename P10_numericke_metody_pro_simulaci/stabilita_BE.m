a = [-0.1, -3, -10, 3];

x0 = 1; h = 1; tf = 10;

x = zeros(1,tf/h+1); x(1) = x0;

for n = 1:length(a)
    G(:,:,n) = ss(a(n),1,1,0);
end

figure(1)
tau = linspace(0,tf,100);

for n = 1:length(a)
    subplot(2,2,n)
    x_analytical = impulse(G(:,:,n),tau);
    t = 0;
    k = 1;
    while t <= tf
        x(k+1) = (1-a(n)*h)\x(k);
        t = t+h;
        k = k+1;
    end
    hf = plot(tau,x_analytical,0:h:tf,x(1:end-1),'o-');
    set(hf,'LineWidth',2)
    legend('Analytical','Backward Euler')
    title(['a=',num2str(a(n))])
end
%     
% print -depsc2 ../../figures/BE_stability.eps
% !epstopdf ../../figures/BE_stability.eps
