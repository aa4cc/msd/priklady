%% Symbolic differentiation of f()

syms t x(t) xk fk

f(t) = (x(t)-2*t*(x(t))^2)/(1+t)
dfdt = simplify(diff(f(t),t))

subs(dfdt,{x(t),diff(x(t),t)},{xk,fk})

%% Numerical solution using higher order Taylor series

%clear all, clc

t0 = 0; tf = 1; x0 = 5; 
t = linspace(t0,tf,1000);
C = logspace(-1,1,30);
x = zeros(length(C),length(t));

for ii = 1:length(C)
    x(ii,:) = (t + 1)./(t.^2 + C(ii));
end

figure(6)
plot(t,x)
xlabel('t [s]')
ylabel('x(t)')

h = 0.0125;

t = t0;
x = x0;
ix = 1;
dxdt = (x-2*t*x^2)/(1+t);
dfdt = (dxdt*(4*x + 1))/(t + 1) - 4*dxdt*x - (x*(2*x + 1))/(t + 1)^2;

xtrue = @(t) (1+t)/(t^2+1/5);  % True solution computed symbolically

while t <= (tf-h)+h/1000
    x(ix+1) = x(ix)+h*dxdt+dfdt*h^2/2;
    t = t+h;
    dxdt = (x(ix)-2*t*x(ix)^2)/(1+t);
    dfdt = (dxdt*(4*x(ix) + 1))/(t + 1) - 4*dxdt*x(ix) - (x(ix)*(2*x(ix) + 1))/(t + 1)^2;
    error(ix) = x(ix)-xtrue(t);
    str = sprintf('ix=%d \t tk=%6.4f \t xk=%6.4f \t xk_true=%6.4f \t error=%6.4f',ix,t,x(ix),xtrue(t),error(ix));
    %disp(str)
    ix = ix+1;
end

global_error1 = error(end)
global_error1/h
relative_global_error1 = global_error1/x(end)


hold on,
hf1 = plot(t0:h:tf,x(1:end),'o-');
set(hf1,'LineWidth',2)
hold off

%print -depsc2 ../../figures/ex_FE_h01.eps
%!epstopdf ../../figures/ex_FE_h01.eps

