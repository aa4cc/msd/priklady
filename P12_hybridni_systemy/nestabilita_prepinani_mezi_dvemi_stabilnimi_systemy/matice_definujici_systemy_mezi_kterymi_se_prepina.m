%% Matice definující dva dynamické systémy d/dt x=A1*x a d/dt x=A2*x, mezi kterými je přepínáno  
% Obě jsou stabilní (vlastní čísla v levé komplexní polorovině).

A1 = [-1, -100; 10, -1]
A2 = [-1, 10; -100, -1]
  
eig(A1)
eig(A2)

    