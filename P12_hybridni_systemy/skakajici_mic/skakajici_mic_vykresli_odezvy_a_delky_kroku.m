%% Vykreslení odezev a délky simulačních kroků po simulaci skákajícího míče v Simulinku

subplot(2,1,1)
plot(logsout.getElement('x').Values,'.-','MarkerSize',12), hold on,
plot(logsout.getElement('v').Values,'.-','MarkerSize',12), hold off
ylabel('x,v')
grid on
subplot(2,1,2)
semilogy(tout(1:end-1), diff(tout),'.-','MarkerSize',12)
grid on
xlabel('t')
ylabel('\Delta t')