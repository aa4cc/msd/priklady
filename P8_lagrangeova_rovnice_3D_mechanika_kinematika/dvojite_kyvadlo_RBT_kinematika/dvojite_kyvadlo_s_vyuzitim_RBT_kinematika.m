%% Modelování dvojitého kyvadla s využitím Robotics Toolbox for Matlab – kinematika
% 

%%
% Předvedeme si: rotační a homogenní transformace, Jakobiány.

clear all, close all, clc

%% Robotics Toolbox for Matlab
% Autorem je v mezinárodní robotické komunitě uznávaný <http://petercorke.com/ Peter Corke>. Ve verzi 10 je možné toolbox stáhnout na  
% <http://petercorke.com/wordpress/toolboxes/robotics-toolbox>.
% Sice se lze docela dost o práci s toolboxem naučit 
% <http://petercorke.com/wordpress/?ddownload=343 manuálu>, avšak Corkeho <http://petercorke.com/wordpress/rvc/ Corke's
% kniha> je tím plným zdrojem pro učení.

%% Rotační a homogenní transformace/matice v RBT
% Toolbox obsahuje řadu základních funkcí relevantních pro modelování robotů, 
% jako jsou i funkce pro vytváření matic rotačních a homogenních transformací. 
% Například rotační matici pro rotaci okolo osy x o zadaný úhel pi/2 je možno 
% vytvořit pomocí 

R = rotx(pi/2)

%% 
% Rotační transformace odpovídající rotační matici může být formulována i v
% širším rámci homogenních trasformací. Ty jsou vyjádřeny maticemi o
% rozměrech 4x4 a rotační matice tvoří jejich levý horní roh (o rozměrech
% 3x3)

R = trotx(pi/2)

%%
% Pouhé posunutí (translace) může být vyjádřeno pomocí homogenní
% transformace následovně

T = transl(1,2,3)

%%
% Reprezantace jak rotace tak i translace pomocí 4x4 matic homogenní
% transformace umožňuje skládání těchto transformací prostým násobením
% matic

TR = T*R

%%
% Všimněme si výše, že právě při této volbě pořadí obou transformací
% dostáváme obecný tvar homogenní transformace (v levém horním rohu 3x3 rotační
% matice, v pravém horním rohu 3x1 vektor posunutí). Pokud pořadí
% prohodímě, ve výsledné matici homogenní transformace už v tom pravém
% horním bloku vektor posunutí nerozpoznáváme.

RT = R*T

%% 
% Velmi užitečnou funkcionalitou toolboxu je, že (téměř) všechny jeho funkce 
% přijímají i symbolické parametry.  

syms theta rx ry rz

R = trotx(theta)
T = transl(rx,ry,rz)

TR = T*R

%%
% Při použití klasické DH konvence může být transformace mezi dvěma po sobě
% následujícími články v sériovém manipulátoru, řekněme článkem s indexem $(i-1)$
% a článkem s indexem $i$ složena ze čtyř kroků
%
% * posunutí o $d_i$ podél $z_{i-1}$
% * rotace o $\theta_i$ okolo $z_{i-1}$
% * posunutí o $a_i$ ve směru nové osy $x_i$
% * rotace o $\alpha_i$ okolo nové osy $x_i$
%
% S využitím Corkeho toolboxu je sestavení výsledné homogenní transformace
% jednoduché.

syms theta(t) d a alpha

T = transl(0,0,d)*trotz(theta(t))*transl(a,0,0)*trotx(alpha)

%%
% Při definici symbolických DH parametrů jsme zjevně výše už rozhodli, že 
% jde o rotační kloub, protože $\theta$ byla určena coby funkce času, 
% zatímco $d$ zůstává konstantní.

%% Dvojité kyvadlo 
% Tolik snad stačí pro základní seznámení s toolboxem, a nyní je čas začít
% s modelováním dvojitého kyvadla.
%
% <<double_pendulum_DH_frames.png>>

syms theta1(t) theta2(t) l1 l2

d1 = 0;
d2 = 0;
a1 = l1;
a2 = l2;
alpha1 = 0;
alpha2 = 0;

%%
% Nejdříve globální souřadný systém (s indexem 0). Máme sice volnost zvolit
% si jej jakkoliv, nicméně nejpohodlnější pro následné uplatnění DH konvence
% bude mít osu $z_0$ horizontální.
%
% Případně můžeme vyzkoušet i možná přirozenější variantu, kdy $z_0$ bude
% vertikálné. Abychom pak mohli následovat DH konvenci, budeme muset
% nejdříve otočit globální souřadný systém o pi/2 okolo osy y0.

T0 = troty(pi/2)

%%
% Teprve po této rotaci následuje klasická DH posloupnost 4 elementárníc
% transformací pro první článek

%T01 = T0*transl(0,0,d1)*trotz(theta1(t))*transl(a1,0,0)*trotx(alpha1)
T01 = transl(0,0,d1)*trotz(theta1(t))*transl(a1,0,0)*trotx(alpha1)

%% 
% a pro druhý článek, přičemž nejdříve si musíme připravit i transformaci
% pro druhý kloub (v něm počátek prvního souřadného systému)

T02 = T01*transl(0,0,d2)*trotz(theta2(t))*transl(a2,0,0)*trotx(alpha2);
T02 = simplify(T02)

%% 
% Souřadnice těžišť je teď možné získat jednoduše pomocí právě
% zadefinovaných homogenních transformací, přičemž ty polohové vektory pro
% obě těžiště mají pouze x-ovou složku nenulovou (a předpokládáme těžiště
% uprostřed délky článku)

rC1 = T01*[-a1/2;0;0;1]; rC1 = rC1(1:3)
rC2 = T02*[-a2/2;0;0;1]; rC2 = rC2(1:3)

%%
% Hodit se ale budou i souřadnice počátků souřadných systémů

rO0 = [0;0;0]
rO1 = T01*[0;0;0;1]; rO1 = rO1(1:3) 
rO2 = T02*[0;0;0;1]; rO2 = rO2(1:3)

%% 
% Rotační transformace lze identifikovat v těch odvozených homogenních
% transformacích

R01 = T01(1:3,1:3)
R02 = T02(1:3,1:3)

%%
% Jednotkové vektory ve směru os z

z0 = [0;0;1]               % pro variantu s osou z0 horizontální
%z0 = [1;0;0]                % pro variantu s osou z0 vertikální
z1 = R01(:,3)               % efektivnější výpočet místo R1*[0;0;1]
z2 = R02(:,3)               % ani vlastně nebudeme potřebovat

%%
% Jakobiány pro úhlovou rychlost (oba klouby jsou rotační)

Jr1 = [z0, [0;0;0]]
Jr2 = [z0, z1]

%%
% Jakobiány pro translační rychlost obou těžišť (oba klouby jsou rotační)

JtC1 = [cross(z0,rC1-rO0), [0;0;0]]
JtC2 = [cross(z0,rC2-rO0), cross(z1,rC2-rO1)]

%% Alternativní přístup v RBT s využitím high-level konstruktorů z podbalíku ETS3
% Jakkoliv je právě předvedený přístup postavený na sestavování matic pro
% rotační a homogenní transformace naprosto správný, Corkeho toolbox nabízí
% jeho zjednodušení. Základem jsou sice opět matice pro rotační a homogenní
% transformace, ale tentokrát implementované coby složitější objekty
% nabízející další funkčnost, jak uvidíme vzápětí.

%%
% Nejdříve si importujeme (pod)balík ETS3. Tento nám umožní volat pro
% konstruktory jako Ry a Tx se zjevným účelem vytvoření rotace okolo y a
% translace podél x. Technický detail: pokud má bt daný parametr rotace
% chápán toolboxem jako zobecněná souřadnice, uzavřemě ho mezi jednoduché
% uvozovky, například 'theta1'.

import ETS3.*

a1 = 1; a2 = 1;                                                 
dpend = Ry(pi/2)*Rz('q1')*Tx(a1)*Rz('q2')*Tx(a2)*Ry(-pi/2)   

%%
% Výsledný objekt není pouhá matice 4x4. Matlab teď pod proměnnou dpend
% chápe nějaký mechanismus složený ze dvou tuhý částí se zobecněnými
% souřadnicemi q1 a q2 coby úhly okolo z-ových os. Můžeme teď tento objekt
% využít kupříkladu pro vizualizaci s využitím interaktivní metody teach
% (přístupné přes tečkovou syntaxi). Toto vizuální ověření je velmi užitečná při kontrole,
% že jsme souřadné systémy zavedli dobře.

% %dpend.teach   % pro účely generování reportu zakomentovat

%% 
% Alternativně, pokud nám nejde o interaktivitu, je možné vygenerovat
% vizualizaci pro konkrétní hodnoty zobecněných souřadnic (v našem případě dvou úhlů) 

dpend.plot([pi/4,0])

%% 
% A to stejné lze samozřejmě provést symbolicky. A tentokrát nehledejme
% kinematický model pouze pro koncový efektor, nýbrž pro těžiště obou
% článků.

import ETS3.*
syms a1 a2 theta1 theta2  

dpend_c1 = Ry(pi/2)*Rz('q1')*Tx(a1/2)
dpend_c2 = Ry(pi/2)*Rz('q1')*Tx(a1)*Rz('q2')*Tx(a2/2)

%%
% Matice reprezentující homogenní transformaci jsou

TT01 = dpend_c1.fkine(theta1)
TT02 = dpend_c2.fkine([theta1 theta2])

%% Translační Jakobiány pro těžiště obou článků
% Skrze atribut t můžeme přistoupit ke translační složce homogenní transformace, 
% tedy první 3 prvky posledního sloupce matice. Nalezení Jakobiánů je pak
% jednoduché

JJtC1 = jacobian(TT01.t,[theta1 theta2])
JJtC2 = jacobian(TT02.t,[theta1 theta2])

%% Rotační Jakobiány
% Nalezení rotačních Jakobiánů není o nic složitější – jednoduše jde vždy o 
% poslední sloupec odpovídající rotační matice.

RR01 = TT01.R
RR02 = TT02.R

%%

JJr1 = [[1;0;0], zeros(3,1)]
JJr2 = [[1;0;0], RR01(:,3)]

%% 
% A do třetice ještě jeden způsob, a opět symbolicky

syms a1 a2 theta1 theta2 

%%
% Klíčové dvě funkce budou Revolute() a SerialLink(). První vytvoří objekt
% reprezentující daný článek a kloub. Ta druhá zkombinuje články a klouby
% dohromady a vytvoří tak model sériového robotického manipulátoru.
% Parametry pro Revolute() jsou přímo DH parametry (d, a, alpha), ale
% stejně tak i hmotnost, moment setrvačnosti a pozice těžiště. Tyto ale
% zatím nepotřebujeme.

% syms m1 m2
% 
% I1zz = m1*a1^2/12;
% I1 = sym(zeros(3,3)); I1(3,3) = I1zz;
% 
% I2zz = m2*a2^2/12;
% I2 = sym(zeros(3,3)); I2(3,3) = I2zz;
% 
% L1 = Revolute('a',a1,'m',m1,'I',I1,'r',[-a1/2;0;0])
% L2 = Revolute('a',a2,'m',m2,'I',I2,'r',[-a2/2;0;0])

L1 = Revolute('a',a1,'r',[-a1/2;0;0])
L2 = Revolute('a',a2,'r',[-a2/2;0;0])

dpendDHs = SerialLink([L1, L2],'name','Double pendulum')