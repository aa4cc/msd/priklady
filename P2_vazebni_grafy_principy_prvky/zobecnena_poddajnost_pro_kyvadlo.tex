\documentclass[a4paper,10pt]{article}

\usepackage[czech]{babel}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}

\usepackage{fullpage}

\usepackage{babel}

\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}


\begin{document}

\section*{Určení modulu (parametru) zobecněné poddajnosti pro kyvadlo}

\subsection*{Zadání}
Uvažujte klasické kyvadlo s hmotným bodem o hmotnosti $m$ zavěšeným na nehmotném závěsu o délce $l$. Uvažujte pouze chování kyvadla okolo rovnovážného stavu (svislé polohy). 

\begin{enumerate}
 \item Najděte hodnotu modulu $C$ zobecněné poddajnosti, která modeluje vliv tíhové síly coby kroutivý moment vracející kyvadlo do svislé polohy.
 \item Najděte hodnotu toho stejného parametru poddajnosti $C$ uvažováním potenciální energie (odvozené od gravitační potenciální energie), spojené s vychýlením z rovnovážné svislé polohy. Nápověda: použijte aproximaci pro funkci $\cos$.
 \item Vyřešte oba předchozí problémy pro situaci, kdy budou hmotné závaží a nehmotný závěs nahrazeny tuhým prutem o délce $l$ a hmotnosti $m_\mathrm{r}$.
\end{enumerate}

[Zdroj: Brown, Neřešené příklady 3.3 a 3.4, strana 84]

\subsection*{Řešení}
Naše představa kyvadla coby prvku modelovaného zobecněnou poddajností je, že kyvadlo je po vychýlení ze svislé polohy vraceneno jakousi fiktivní torzní pružinou. Pojďme tedy hledat popis takové pružiny. 

Prvek typu zobecněná poddajnost je modelován pomocí (obecně nelineárního) vztahu mezi zobecněnou silou $e$ a zobecněným posunutím $q$. V našem případě rotačního pohybu je instancí zobecněné síly kroutivý moment $M$ okolo osy závěsu a zobecněným posunutím je úhel $\theta $ vychýlení kyvadla z rovnovážné svislé pozice. Hledáme tedy vztah mezi momentem a úhlem.

(Tíhová) síla působící na kyvadlo je $F_\mathrm{g} = m\mathrm g$ a její průmět do směru tečny k pohybu kyvadla je $F_\mathrm{g} \sin(\theta)$. Vynásobením délkou ramene pak získáváme moment síly

\begin{equation}
M = F_\mathrm{g} \sin(\theta) l. 
\end{equation}

Ale ten už je přesně v tom tvaru, jak pro obecné (nelineární) prvky čekáme -- $M$ je funkcí $\theta$, tedy zobecněná síla je funkcí zobecněného posunutí. Takže vlastně taková nelineární pružina. Dává to smysl, že se nám tu nepodařilo identifikovat jedinou konstantu tuhosti pružiny (coby převrácenou hodnotu toho hledaného modulu poddajnosti), protože zjevně při vychýlení do $90^\circ$ bude na kyvadlo působit větší kroutivý moment než při vychýlení jen $10^\circ$. 

Nicméně, jestliže zadáno bylo modelovat chování jen okolo svislého rovnovážného stavu, tedy pro $\theta\approx 0$, tak můžeme využít aproximace $\sin(\theta)\approx \theta$ a pro závislost momentu na úhlovém vychýlení můžeme psát

\begin{equation}
 M = m\mathrm{g}l \theta. 
\end{equation}

Identifikovat module poddajnosti je jednoduché, když si připomeneme, že v obecnosti platí $e = \frac{1}{C}q$, a tedy v našem případě

\begin{equation*}
 M = \underbrace{m\mathrm{g}l}_{1/C} \theta, 
\end{equation*}
tedy 

\begin{equation}\boxed{
 C = \frac{1}{m\mathrm{g}l}.}
\end{equation}

Hotovo. Pojďme ale to stejné zadání splnit ještě i druhým přístupem, který je ve skutečnosti univerzálnější (jak uvidíme ještě i později v předmětu) -- budeme hledat celkovou energii v prvku a na základě znalosti obecného vztahu pro (potenciální) energii v prvku typu zobecněná poddajnost se budeme snažit identikovat ten modul poddajnosti. Připomeňme si tedy, že obecně platí

\begin{equation}
 \mathcal{V}(q) = \int_0^q e(\tilde{q})\mathrm{d}\tilde{q},
\end{equation}
tedy v našem případě

\begin{equation}
 \mathcal{V}(\theta) = \int_0^\theta M(\tilde{\theta})\mathrm{d}\tilde{\theta}.
\end{equation}

Vskutku, potřebujeme tu energii vyjádřit coby funkci úhlu $\theta$. Zbývá tedy vyjádřit ten integrál. Mezivýsledky pro to máme už z toho předchozího řešení -- umíme vyjádřit moment coby funkci úhlu 
\begin{align}
 \mathcal{V}(\theta) &= \int_0^\theta M(\tilde{\theta})\mathrm{d}\tilde{\theta}\nonumber\\
 &= \int_0^\theta m\mathrm{g}\sin(\tilde{\theta}l)\mathrm{d}\tilde{\theta}\nonumber\\
 &= m\mathrm{g}l[-\cos(\tilde\theta)]_{0}^{\theta}\nonumber\\
 &= m\mathrm{g}l (1-\cos(\theta)).
\end{align}

Z takového výsledku ale bezprostřední užitek nemáme, protože zjevně nesedí na ten obecný tvar pro potenciální energii v lineárním prvku $\mathcal{V}(q) = \frac{1}{2}\frac{1}{C}q^2$. To ale není žádné překvapení, protože bez toho omezení se na pohyb pouze v okolí svislé pozice je náš prvek nelineární. A pro to omezení se na malé úhly potřebujeme aproximovat funkci $\cos$. 

Snad nás může napadnout použít notoricky známé aproximace $\cos(\theta)\approx 1$ pro $\theta \approx 0$. Toto ale zjevně nepomůže, protože potom by potenciální energie vyšla nulová! Co s tím? Jednoduše musíme v té aproximaxi funkce $\cos$ přidat další člen v Taylorově rozvoji. Připomeňme si, že druhý člen v takovém Taylorově rozvoji je roven nule nenulový je až třetí člen, konkrétně $\cos(\theta) \approx 1 - \frac{1}{2}\theta^2$. Po dosazení do vztahu pro potenciální energii dostáváme

\begin{align*}
 \mathcal{V}(\theta) &= m\mathrm{g}l (1-1+\frac{1}{2}\theta^2)\nonumber\\
 &= \frac{1}{2}m\mathrm{g}l \theta^2.
\end{align*}

Srovnáním tohoto výsledku s obecným vztahem můžeme identifikovat člen $1/C$
\begin{equation*}
 \mathcal{V}(\theta) =  \frac{1}{2}\underbrace{m\mathrm{g}l}_{1/C} \theta^2,
\end{equation*}
tedy
\begin{equation}\boxed{
 C = \frac{1}{m\mathrm{g}l},}
\end{equation}
což souhlasí s výsledkem, který jsme si před chvílí odvodili jiným způsobem.

A teď ještě celý výpočet zopakujeme pro situaci, kdy bude kyvadlo představováno nikoliv hmotným bodem na nehmotném závěsu, nýbrž tenkou tyčí o stejné hmotnosti $m$ a délce $l$. 

Potenciální energie získaná vychýlením kyvadla ze svislé rovnovážné polohy je funkcí změny  polohy (výšky) těžiště. Jinými slovy, z hlediska určení potenciální energie složitost tvaru kyvadla vlastně můžeme ignorovat do té míry, pokud umíme určit polohu těžiště. V případě dlouhé tyče z homogenního materiálu je to jednoduché -- těžiště se nachází přesně uprostřed. A tím jsme úlohu na první pohled složitější převedli na výše vyřešený problém kyvadla o hmotnosti $m$ a délce $l/2$. Tedy plný nelineární vztah pro moment síly

\begin{equation}
M = m\mathrm{g} \sin(\theta) \frac{l}{2} 
\end{equation}
a jeho linearizace v okolí svislé polohy
\begin{equation}
M = m\mathrm{g}  \frac{l}{2} \theta, 
\end{equation}
z čehož plyne, že modul poddajnosti je
\begin{equation}\boxed{
C = \frac{1}{\frac{1}{2}m\mathrm{g}l}. }
\end{equation}


\end{document}
