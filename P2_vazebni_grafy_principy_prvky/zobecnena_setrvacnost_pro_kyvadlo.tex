\documentclass[a4paper,10pt]{article}

\usepackage[czech]{babel}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}

\usepackage{fullpage}

\usepackage{babel}

\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}


\begin{document}

\section*{Určení modulu (parametru) zobecněné setrvačnosti pro kyvadlo}

\subsection*{Zadání}
Uvažujte klasické kyvadlo s hmotným bodem o hmotnosti $m$ zavěšeným na nehmotném závěsu o délce $l$. V tomto příkladu ale neuvažujte vliv gravitace -- to kyvadlo bude provozováno v horizontální rovině (ten závěs pak zjevně nemůže být realizován jen provázkem nýbrž nějakým tuhým prutem, jehož hmotnost ale v první chvíli zanedbáváme).

\begin{enumerate}
 \item Najděte moment potřebný k vyvození zadaného úhlového zrychlení. Tímto způsobem odvoďte hotnotu parametru zobecněné setrvačnosti vzhledem k úhlové rychlosti.
 \item Najděte hodnotu parametru zobecněné setrvačnosti vzhledem k úhlové rychlosti nalezením vztahu pro kinetickou energii. Samozřejmě byste měli dostat stejný výsledek jako předtím. 
 \item Místo hmotného bodu na nehmotném závěsu uvažujte dlouhou tenkou tyč a zopakujte předchozí výpočty.
\end{enumerate}

[Neřešené příklady 3.11 a 3.12 z Browna (strana 91, 92): zobecněná setrvačnost u mechanického systému - kyvadlo]

\subsection*{Řešení}
Ideální prvek typu zobecněná setrvačnost je modelován vztahem $p = I f$. Pro rotační mechanický pohyb se tento vztah specializuje na 
\begin{equation*}
 p = J \omega,
\end{equation*}
kde $p$ je moment hybnosti, $J$ je moment setrvačnosti a $omega$ je úhlová rychlost. 

Derivací obou stran rovnice podle času dostáváme
\begin{equation*}
 \dot p = J \dot\omega,
\end{equation*}
což je rotační verze druhého Newtonova zákona -- připomeňme si, že derivací zobecněné hybnosti je zobecněná síla, v tomto případě momemt síly
\begin{equation*}
 M = J \dot\omega.
\end{equation*}

Fyzikální parametr, který teď máme pro popis systému k dispozici, je hmotnost, a ta je určuje translační pohyb. Budeme se tedy snažit převést výše uvedenou rotační verzi druhého Newtonova zákona do translační verze. Rozepíšeme tedy moment síly na levé straně pomocí parametrů a veličin relevantních pro translační pohyb

\begin{align*}
M &=  F l\\
&= (m\dot v) l\\
&= (m\dot\omega l) l\\
&= ml^2\dot\omega
\end{align*}
kde druhý řádek jsme získali uplatněním translační verze druhého Newtonova zákona (jen pro účely výkladu zvýrazňujeme použitím závorek) a třetí dosazením vztahu mezi translační a úhlovou rychlostí. 

Srovnáním s obecným předpisem pro vztah mezi momentem setrvačnosti a úhlovou rychlostí identifikujeme moment setrvačnosti 
\begin{equation*}\boxed{
 J = m l^2.}
\end{equation*}

A teď to stejné ještě jednou a jiným přístupem -- určením kinetické energie. Pro translační pohyb hmotného bodu platí notoricky známý vztah 
\begin{equation*}
 \mathcal{T}(v) = \frac{1}{2}mv^2,
\end{equation*}
nicméně my budeme chtít mít tu energii vyjádřenu coby funkci nikoliv translační nýbrž rotační rychlosti
\begin{align*}
 \mathcal{T}(\omega) &= \frac{1}{2}m(\omega l)^2\nonumber\\
 &= \frac{1}{2}(ml^2)\omega^2.
\end{align*}

Identifikovat v této konkrétní instanci obecného vztahu $\mathcal{T} = \frac{1}{2}If^2$ modul zobecněné setrvačnosti je jednoduché
\begin{equation*}\boxed{
 J = m l^2.}
\end{equation*}

Tedy shoda s výsledkem předchozího postupu. A vskutku, tento druhý přístup založený na nalezení energie je velmi efektivní, a jeho síla se ukáže, až budeme hledat ekvivalentní moduly pro všelijak propojené základní prvky. 

Nakonec ještě místo hmotného bodu na nehmotném závěsu uvažujme trochu jiné kyvadlo: dlouho tenkou tyč. Stejná délka $l$ jako předtím měl ten závěs, a stejná hmotnost $m$ jako měl předtím ten hmotný bod. To odvození provedeme opět oběma postupy.

Je-li moment síly (či spíše jeho velikost, ať tu v tomto jednoduchém příkladu nemusíme řešit vektorové záležitosti) dána součinem délky ramene a velikosti (kolmo působící) síly, pak v případě v prostoru rozložené hmoty (tyčovitého kyvadla) bude \textit{celkový} dán integrací přes všechny elementární částečky objemu v celém objemu kyvadla. V případě tenké dlouhé tyče můžeme dvě dimenze zanedbat (za připuštění chyby takové aproximace) a integraci uvažovat jen v jednom -- podélném -- směru (obecně by to však byla integrace přes objem) 

\begin{align*}
 p &= \int_0^l x \underbrace{v(x)}_{\omega x} \underbrace{\rho(x) \mathrm{d}x}_{\mathrm{d}m}\\
 &= \int_0^l x^2 \omega \rho(x)\mathrm{d}x\\
 &= \int_0^l x^2 \rho(x) \mathrm{d}x \;\omega,
\end{align*}
kde zjevně je počátek souřadného systému umístěn do osy otáčení; hustota $\rho$ je vztažena k délkové souřadnici (tedy její jednotkou bude $\mathrm{kg}/\mathrm{m}$); a dále pak vyjmutí úhlové rychlosti $\omega$ z integrálu, jak to děláme na třetím řádku, je možné, jelikož pro všechny elementry je u tuhé tyče shodná (na rozdíl od rychlosti translační).

Tedy moment setrvačnosti je 
\begin{equation}
 J = \int_0^l x^2 \rho(x) \mathrm{d}x, 
\end{equation}
což se v případě homogenního materiálu redukuje na
\begin{align*}
 J &= \rho\int_0^l x^2 \mathrm{d}x\\
 &= \rho \left[\frac{1}{3}x^3\right]_0^l\\
 &= \frac{1}{3}\rho l^3\\
 &= \frac{1}{3}m l^2.
\end{align*}

Tedy 
\begin{equation}\boxed{
 J = \frac{1}{3}m l^2}.
\end{equation}

Postup založený na výpočetu kinetické energie bude hodně podobný (možná, že dokonce ještě přímočařejší) a hlavně vede ke stejnému výsledku

\begin{align*}
 \mathcal{T} &= \int \frac{1}{2}\rho v^2 \mathrm{d}x\\
 &= \frac{1}{2} \underbrace{\int \rho x^2 \mathrm{d}x}_{J} \; \omega^2.
\end{align*}


\end{document}

