%% Modelování dvojitého kyvadla s využitím Robotics Toolbox for Matlab – dynamika
% 

%%
% Předvedeme si: rotační a homogenní transformace, Jakobiány.

clear all, close all, clc

%% Robotics Toolbox for Matlab
% Autorem je v mezinárodní robotické komunitě uznávaný <http://petercorke.com/ Peter Corke>. Ve verzi 10 je možné toolbox stáhnout na  
% <http://petercorke.com/wordpress/toolboxes/robotics-toolbox>.
% Sice se lze docela dost o práci s toolboxem naučit 
% <http://petercorke.com/wordpress/?ddownload=343 manuálu>, avšak Corkeho <http://petercorke.com/wordpress/rvc/ Corke's
% kniha> je tím plným zdrojem pro učení.

%% Dvojité kyvadlo 
% Tolik snad stačí pro základní seznámení s toolboxem, a nyní je čas začít
% s modelováním dvojitého kyvadla.
%
% <<double_pendulum_DH_frames.png>>

%% Zobecněné souřadnice (kloubové úhly)

%theta = sym('theta',[2,1],'real') 

syms theta1 theta2 theta1dot theta2dot

%% DH parametry 

l1 = 0.18;      % délka prvního kyvadla
l2 = 0.15;      % délka druhého kyvadla

d1 = 0;
d2 = 0;
a1 = l1;
a2 = l2;
alpha1 = 0;
alpha2 = 0;

%% Globální souřadný systém
% Nejdříve globální souřadný systém (s indexem 0). Máme sice volnost zvolit
% si jej jakkoliv, nicméně nejpohodlnější pro následné uplatnění DH konvence
% bude mít osu $z_0$ horizontální.
%
% Uvidíme v následujícím, že ty vysokoúrovňové konstruktory v RBT uvažují,
% že z0 míří kolmo vzhůru. Pro toto se ale v toolboxu nabízí možnost transformace 
% souřadného systému báze, v tomto příadě otočením o pi/2 okolo osy y0.


%% Parametry pro dynamiku
% Konkrétní hmotnosti a momenty setrvačnosti odpovídají našemu dvojitému
% kyvadlu (na rotační bázi – Furutovo kyvadlo)

m1 = 0.0356; 
m2 = 0.0435;

I1xx = 0.0000025;
I1yy = 0.000117;
I1zz = 0.000117;

I2xx = 0.00000316;
I2yy = 0.000103;
I2zz = 0.000103;

%%
% Ale bylo by možné ty momenty setrvačnosti odhadnout i vlastním výpočtem
% (uvažovali bychom tvar jednoduchého podlouhlého kvádru)

b1 = 0.0155;      % další dva rozměry kyvadla (podlouhlého kvádru)
c1 = 0.0155;      

b2 = 0.015;
c2 = 0.015;

% I1xx = m1*(b1^2+c1^2)/12;
% I1yy = m1*(a1^2+c1^2)/12;
% I1zz = m1*(a1^2+b1^2)/12;

% I2xx = m2*(b2^2+c2^2)/12;
% I2yy = m2*(a2^2+c2^2)/12;
% I2zz = m2*(a2^2+b2^2)/12;

%% 
% Výsledné matice pro momenty setrvačnosti jsou

I1 = diag([I1xx,I1yy,I1zz]);
I2 = diag([I2xx,I2yy,I2zz]);

%% Konstrukce modelu
% Klíčové dvě funkce budou Revolute() a SerialLink(). První vytvoří objekt
% reprezentující daný článek a kloub. Ta druhá zkombinuje články a klouby
% dohromady a vytvoří tak model sériového robotického manipulátoru.
% Parametry pro Revolute() jsou přímo DH parametry (d, a, alpha), ale
% stejně tak i hmotnost, moment setrvačnosti a pozice těžiště.

L1 = Revolute('a',a1,'m',m1,'I',I1,'r',[-a1/2;0;0])
L2 = Revolute('a',a2,'m',m2,'I',I2,'r',[-a2/2;0;0])

dpendDHs = SerialLink([L1, L2],'name','Double pendulum')

%%
% Můžeme si i konfiguraci robota vykreslit

dpendDHs.plot([pi/4,0])

%%
% Vidíme ale, že skutečně ten toolbox má jako výchozí nastavení, že osa z0
% je kolmo nahoru. Jednoduchým způsobem, jak tu bázi přeorientovat, je
% zahrnout transformační matici přímo v definici celého systému. 

T0 = troty(pi/2)

dpendDHs = SerialLink([L1, L2],'name','Double pendulum','base',T0)

%%
% Po vykreslení konfigurace vidíme, že je vše už v pořádku

dpendDHs.plot([pi/4,0])

%% Maticové koeficienty Lagrangeovy rovnice

%% Matice setrvačnosti

M = dpendDHs.inertia([theta1,theta2])

%% Coriolisova matice

C = dpendDHs.coriolis([theta1,theta2],[theta1dot,theta2dot])

%% Vektor zatížení odpovídající gravitaci

G = dpendDHs.gravload([theta1,theta2]).'
