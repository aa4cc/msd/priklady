%% Modelování dvojitého kyvadla s využitím Robotics Toolbox for Matlab – dynamika
% 

%%
% Předvedeme si: rotační a homogenní transformace, Jakobiány.

clear all, close all, clc

%% Robotics Toolbox for Matlab
% Autorem je v mezinárodní robotické komunitě uznávaný <http://petercorke.com/ Peter Corke>. Ve verzi 10 je možné toolbox stáhnout na  
% <http://petercorke.com/wordpress/toolboxes/robotics-toolbox>.
% Sice se lze docela dost o práci s toolboxem naučit 
% <http://petercorke.com/wordpress/?ddownload=343 manuálu>, avšak Corkeho <http://petercorke.com/wordpress/rvc/ Corke's
% kniha> je tím plným zdrojem pro učení.

%% Rotační a homogenní transformace/matice v RBT
% Toolbox obsahuje řadu základních funkcí relevantních pro modelování robotů, 
% jako jsou i funkce pro vytváření matic rotačních a homogenních transformací. 
% Například rotační matici pro rotaci okolo osy x o zadaný úhel pi/2 je možno 
% vytvořit pomocí 

R = rotx(pi/2)

%% 
% Rotační transformace odpovídající rotační matici může být formulována i v
% širším rámci homogenních trasformací. Ty jsou vyjádřeny maticemi o
% rozměrech 4x4 a rotační matice tvoří jejich levý horní roh (o rozměrech
% 3x3)

R = trotx(pi/2)

%%
% Pouhé posunutí (translace) může být vyjádřeno pomocí homogenní
% transformace následovně

T = transl(1,2,3)

%%
% Reprezantace jak rotace tak i translace pomocí 4x4 matic homogenní
% transformace umožňuje skládání těchto transformací prostým násobením
% matic

TR = T*R

%%
% Všimněme si výše, že právě při této volbě pořadí obou transformací
% dostáváme obecný tvar homogenní transformace (v levém horním rohu 3x3 rotační
% matice, v pravém horním rohu 3x1 vektor posunutí). Pokud pořadí
% prohodímě, ve výsledné matici homogenní transformace už v tom pravém
% horním bloku vektor posunutí nerozpoznáváme.

RT = R*T

%% 
% Velmi užitečnou funkcionalitou toolboxu je, že (téměř) všechny jeho funkce 
% přijímají i symbolické parametry.  

syms theta rx ry rz

R = trotx(theta)
T = transl(rx,ry,rz)

TR = T*R

%%
% Při použití klasické DH konvence může být transformace mezi dvěma po sobě
% následujícími články v sériovém manipulátoru, řekněme článkem s indexem $(i-1)$
% a článkem s indexem $i$ složena ze čtyř kroků
%
% * posunutí o $d_i$ podél $z_{i-1}$
% * rotace o $\theta_i$ okolo $z_{i-1}$
% * posunutí o $a_i$ ve směru nové osy $x_i$
% * rotace o $\alpha_i$ okolo nové osy $x_i$
%
% S využitím Corkeho toolboxu je sestavení výsledné homogenní transformace
% jednoduché.

syms theta(t) d a alpha

T = transl(0,0,d)*trotz(theta(t))*transl(a,0,0)*trotx(alpha)

%%
% Při definici symbolických DH parametrů jsme zjevně výše už rozhodli, že 
% jde o rotační kloub, protože $\theta$ byla určena coby funkce času, 
% zatímco $d$ zůstává konstantní.

%% Dvojité kyvadlo 
% Tolik snad stačí pro základní seznámení s toolboxem, a nyní je čas začít
% s modelováním dvojitého kyvadla.
%
% <<double_pendulum_DH_frames.png>>

syms t
assume(t,'real')

syms theta1(t) 
syms theta2(t)

assume(theta1(t),'real')
assumeAlso(theta2(t),'real')        % Nutné použít assumeAlso, jinak bude 
                                    % vyresetován předpoklad na theta1.
                                    % Důvodem je, že theta1(t) je výraz a
                                    % ne funkce a skrze t je spojení s theta2(t).
                                    % Funkce jsou theta1 a theta2.

theta = [theta1(t); theta2(t)]      % theta je vektor symbolických výrazů.

syms l1 l2

assume(l1,{'positive','real'})
assume(l2,{'positive','real'})

syms l1 l2

assume(l1,{'positive','real'})
assume(l2,{'positive','real'})

d1 = 0;
d2 = 0;
a1 = l1;
a2 = l2;
alpha1 = 0;
alpha2 = 0;

%%
% Nejdříve globální souřadný systém (s indexem 0). Máme sice volnost zvolit
% si jej jakkoliv, nicméně nejpohodlnější pro následné uplatnění DH konvence
% bude mít osu $z_0$ horizontální.
%
% Případně můžeme vyzkoušet i možná přirozenější variantu, kdy $z_0$ bude
% vertikálné. Abychom pak mohli následovat DH konvenci, budeme muset
% nejdříve otočit globální souřadný systém o pi/2 okolo osy y0.

T0 = troty(pi/2)

%%
% Teprve po této rotaci následuje klasická DH posloupnost 4 elementárních
% transformací pro první článek

%T01 = T0*transl(0,0,d1)*trotz(theta1(t))*transl(a1,0,0)*trotx(alpha1)
T01 = transl(0,0,d1)*trotz(theta(1))*transl(a1,0,0)*trotx(alpha1)

%% 
% a pro druhý článek, přičemž nejdříve si musíme připravit i transformaci
% pro druhý kloub (v něm počátek prvního souřadného systému)

T02 = T01*transl(0,0,d2)*trotz(theta(2))*transl(a2,0,0)*trotx(alpha2);
T02 = simplify(T02)

%% 
% Souřadnice těžišť je teď možné získat jednoduše pomocí právě
% zadefinovaných homogenních transformací, přičemž ty polohové vektory pro
% obě těžiště mají pouze x-ovou složku nenulovou (a předpokládáme těžiště
% uprostřed délky článku)

rC1 = T01*[-a1/2;0;0;1]; rC1 = rC1(1:3)
rC2 = T02*[-a2/2;0;0;1]; rC2 = rC2(1:3)

%%
% Hodit se ale budou i souřadnice počátků souřadných systémů

rO0 = [0;0;0]
rO1 = T01*[0;0;0;1]; rO1 = rO1(1:3) 
rO2 = T02*[0;0;0;1]; rO2 = rO2(1:3)

%% 
% Rotační transformace lze identifikovat v těch odvozených homogenních
% transformacích

R01 = T01(1:3,1:3)
R02 = T02(1:3,1:3)

%%
% Jednotkové vektory ve směru os z

z0 = [0;0;1]               % pro variantu s osou z0 horizontální
%z0 = [1;0;0]                % pro variantu s osou z0 vertikální
z1 = R01(:,3)               % efektivnější výpočet místo R1*[0;0;1]
z2 = R02(:,3)               % ani vlastně nebudeme potřebovat

%%
% Jakobiány pro úhlovou rychlost (oba klouby jsou rotační)

Jr1 = [z0, [0;0;0]]
Jr2 = [z0, z1]

%%
% Jakobiány pro translační rychlost obou těžišť (oba klouby jsou rotační)

JtC1 = [cross(z0,rC1-rO0), [0;0;0]]
JtC2 = [cross(z0,rC2-rO0), cross(z1,rC2-rO1)]

%% Potenciální energie
% Hmotnosti
syms m1 m2 g

assume(m1,{'positive','real'});
assume(m2,{'positive','real'});
assume(g,{'positive','real'});

V1 = m1*[-g;0;0]'*rC1;       % předpokládá, že směru "dolů" ve směru osy x
V2 = m2*[-g;0;0]'*rC2;
V = V1+V2 

%% Momenty setrvačnosti
syms I1xx I1yy I1zz I2xx I2yy I2zz

assume(I1xx,{'positive','real'});
assume(I1yy,{'positive','real'});
assume(I1zz,{'positive','real'});
assume(I2xx,{'positive','real'});
assume(I2yy,{'positive','real'});
assume(I2zz,{'positive','real'});

I1 = diag([I1xx, I1yy, I1zz])
I2 = diag([I2xx, I2yy, I2zz])

%% Pohybová rovnice
% Nebudeme ani už sestavovat obecnou Lagrangeovu rovnici, ale přímo už
% budeme sestatovat maticové členy pro pohybovou rovnici
%
% $$
% M(q) \ddot q + C(q,\dot q) \dot q + G(q) = Q
% $$
%
% kde G(q) je gradient potenciální energie vzhledem k q.


%% Matice setrvačnosti

M = m1*JtC1'*JtC1 + m2*JtC2'*JtC2 + Jr1'*R01*I1*R01'*Jr1 + Jr2'*R02*I2*R02'*Jr2;
M = simplify(M)

%% Coriolisova matice 

C = sym(zeros(2,2));

for k=1:2
    for j=1:2
        for i=1:2
            C(k,j) = C(k,j) + 1/2*(diff(M(k,j),theta(i))+diff(M(k,i),theta(j))-diff(M(i,j),theta(k)))*diff(theta(i),t);
        end
    end
end

C

%% Vektor zatížení odpovídající tíhové síle
          
g1 = diff(V,theta(1));
g2 = diff(V,theta(2));

G = [g1; g2]

%% Tření

syms b1 b2

f1 = b1*diff(theta(1),t)
f2 = b2*diff(theta(2),t)

F = [f1; f2]