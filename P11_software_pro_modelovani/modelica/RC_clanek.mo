model RC_clanek
  Modelica.Electrical.Analog.Basic.Ground ground1 annotation(
    Placement(visible = true, transformation(origin = { -32, -36}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Electrical.Analog.Basic.Resistor resistor1(R = 10000, useHeatPort = false)  annotation(
    Placement(visible = true, transformation(origin = {4, 24}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Electrical.Analog.Basic.Capacitor capacitor1(C = 0.000001, v(start = 0.1))  annotation(
    Placement(visible = true, transformation(origin = {44, 2}, extent = {{-10, -10}, {10, 10}}, rotation = -90)));
  Modelica.Electrical.Analog.Sources.SineVoltage sineVoltage1(V = 220, freqHz = 50)  annotation(
    Placement(visible = true, transformation(origin = {-32, 8}, extent = {{-10, -10}, {10, 10}}, rotation = -90)));
equation
  connect(sineVoltage1.n, ground1.p) annotation(
    Line(points = {{-32, -2}, {-32, -2}, {-32, -26}, {-32, -26}, {-32, -26}}, color = {0, 0, 255}));
  connect(sineVoltage1.p, resistor1.p) annotation(
    Line(points = {{-32, 18}, {-32, 18}, {-32, 24}, {-6, 24}, {-6, 24}}, color = {0, 0, 255}));
  connect(resistor1.n, capacitor1.p) annotation(
    Line(points = {{14, 24}, {44, 24}, {44, 12}}, color = {0, 0, 255}));
  connect(capacitor1.n, ground1.p) annotation(
    Line(points = {{44, -8}, {-32, -8}, {-32, -26}, {-32, -26}}, color = {0, 0, 255}));
  annotation(
    uses(Modelica(version = "3.2.2")));
end RC_clanek;