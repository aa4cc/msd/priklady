model dvojite_kyvadlo
  parameter Real L1=0.18 "Delka horniho clanku";
  parameter Real L2=0.15 "Delka dolniho clanku";
  parameter Real d1=0.015 "Vnejsi sirka horniho clanku";
  parameter Real v1=0.013 "Vnitrni sirka horniho clanku";
  parameter Real d2=0.0155 "Vnejsi sirka dolniho clanku";
  parameter Real v2=0.0135 "Vnitrni sirka dolniho clanku";
  inner Modelica.Mechanics.MultiBody.World world annotation(
    Placement(visible = true, transformation(origin = {-84, 22}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Mechanics.MultiBody.Joints.Revolute kloub_1(phi(displayUnit = "deg", fixed = true, start = 0.3490658503988659), useAxisFlange = true, w(fixed = true))  annotation(
    Placement(visible = true, transformation(origin = {-46, 22}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Mechanics.MultiBody.Joints.Revolute kloub_2(phi(displayUnit = "deg", fixed = true, start = -0.5235987755982988), useAxisFlange = true, w(fixed = true))  annotation(
    Placement(visible = true, transformation(origin = {18, -2}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Mechanics.Rotational.Components.Damper treni_1(d = 0.0001, phi_rel(displayUnit = "rad"))  annotation(
    Placement(visible = true, transformation(origin = {-44, 58}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Mechanics.Rotational.Components.Damper treni_2(d = 0.0001, phi_rel(displayUnit = "rad"))  annotation(
    Placement(visible = true, transformation(origin = {16, 28}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Mechanics.MultiBody.Parts.BodyBox horni_clanek(density = 2700,height = d1, innerHeight = v1, innerWidth = v1,r={0,-L1,0}, width = d1)  annotation(
    Placement(visible = true, transformation(origin = {-18, 8}, extent = {{-10, -10}, {10, 10}}, rotation = -90)));
  Modelica.Mechanics.MultiBody.Parts.BodyBox dolni_clanek(density = 2700,height = d2, innerHeight = v2, innerWidth = v2,r={0,-L2,0}, width = d2)  annotation(
    Placement(visible = true, transformation(origin = {46, -32}, extent = {{-10, -10}, {10, 10}}, rotation = -90)));
equation
  connect(kloub_2.axis, treni_2.flange_b) annotation(
    Line(points = {{18, 8}, {26, 8}, {26, 28}}));
  connect(kloub_2.support, treni_2.flange_a) annotation(
    Line(points = {{12, 8}, {6, 8}, {6, 28}}));
  connect(kloub_1.axis, treni_1.flange_b) annotation(
    Line(points = {{-46, 32}, {-34, 32}, {-34, 58}, {-34, 58}}));
  connect(kloub_1.support, treni_1.flange_a) annotation(
    Line(points = {{-52, 32}, {-54, 32}, {-54, 58}, {-54, 58}}));
  connect(world.frame_b, kloub_1.frame_a) annotation(
    Line(points = {{-74, 22}, {-56, 22}}, color = {95, 95, 95}));
  connect(kloub_1.frame_b, horni_clanek.frame_a) annotation(
    Line(points = {{-36, 22}, {-18, 22}, {-18, 18}}, color = {95, 95, 95}));
  connect(horni_clanek.frame_b, kloub_2.frame_a) annotation(
    Line(points = {{-18, -2}, {8, -2}}));
  connect(kloub_2.frame_b, dolni_clanek.frame_a) annotation(
    Line(points = {{28, -2}, {46, -2}, {46, -22}}));
  annotation(uses(Modelica(version = "3.2.2")));
end dvojite_kyvadlo;