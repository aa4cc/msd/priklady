model dc_motor_with_velocity_feedback "DC motor with a load created in Dymola"
  Modelica.Blocks.Sources.Step step(height = 2, startTime = 0.5) annotation(Placement(visible = true, transformation(extent = {{-136, 2}, {-116, 22}}, rotation = 0)));
  Modelica.Electrical.Analog.Basic.Ground ground annotation(Placement(visible = true, transformation(extent = {{-30, -38}, {-10, -18}}, rotation = 0)));
  Modelica.Electrical.Analog.Basic.EMF emf(k = 0.29) annotation(Placement(visible = true, transformation(extent = {{4, 4}, {24, 24}}, rotation = 0)));
  Modelica.Electrical.Analog.Basic.Inductor inductor(L = 0.06) annotation(Placement(visible = true, transformation(extent = {{-12, 30}, {8, 50}}, rotation = 0)));
  Modelica.Electrical.Analog.Basic.Resistor resistor(R = 18) annotation(Placement(visible = true, transformation(extent = {{-40, 30}, {-20, 50}}, rotation = 0)));
  Modelica.Mechanics.Rotational.Components.Inertia rotor(J = 0.00086564) annotation(Placement(visible = true, transformation(extent = {{32, 4}, {52, 24}}, rotation = 0)));
  Modelica.Mechanics.Rotational.Sensors.SpeedSensor speedSensor annotation(Placement(visible = true, transformation(origin = {64, -60}, extent = {{10, -10}, {-10, 10}}, rotation = 0)));
  Modelica.Blocks.Math.Feedback feedback annotation(Placement(visible = true, transformation(extent = {{-108, 2}, {-88, 22}}, rotation = 0)));
  Modelica.Mechanics.Rotational.Components.Gearbox gear(ratio = 100)  annotation(
    Placement(visible = true, transformation(origin = {70, 14}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Mechanics.Rotational.Components.Inertia load(J = 0.02)  annotation(
    Placement(visible = true, transformation(origin = {98, 14}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Electrical.Analog.Sources.SignalVoltage signalVoltage1 annotation(
    Placement(visible = true, transformation(origin = {-46, 12}, extent = {{10, -10}, {-10, 10}}, rotation = 90)));
  Modelica.Blocks.Continuous.PI PI(T = 0.2, k = 100)  annotation(
    Placement(visible = true, transformation(origin = {-76, 12}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
equation
  connect(PI.y, signalVoltage1.v) annotation(
    Line(points = {{-65, 12}, {-53, 12}}, color = {0, 0, 127}));
  connect(feedback.y, PI.u) annotation(
    Line(points = {{-89, 12}, {-88, 12}}, color = {0, 0, 127}));
  connect(step.y, feedback.u1) annotation(
    Line(points = {{-115, 12}, {-106, 12}}, color = {0, 0, 127}));
  connect(speedSensor.w, feedback.u2) annotation(
    Line(points = {{53, -60}, {-98, -60}, {-98, 4}}, color = {0, 0, 127}));
  connect(ground.p, signalVoltage1.n) annotation(
    Line(points = {{-20, -18}, {-46, -18}, {-46, 2}, {-46, 2}}, color = {0, 0, 255}));
  connect(signalVoltage1.p, resistor.p) annotation(
    Line(points = {{-46, 22}, {-46, 22}, {-46, 40}, {-40, 40}, {-40, 40}}, color = {0, 0, 255}));
  connect(load.flange_b, speedSensor.flange) annotation(
    Line(points = {{108, 14}, {140, 14}, {140, -60}, {74, -60}}));
  connect(gear.flange_b, load.flange_a) annotation(
    Line(points = {{80, 14}, {88, 14}}));
  connect(rotor.flange_b, gear.flange_a) annotation(
    Line(points = {{52, 14}, {60, 14}}));
  connect(emf.flange, rotor.flange_a) annotation(
    Line(points = {{24, 14}, {32, 14}}));
  connect(ground.p, emf.n) annotation(
    Line(points = {{-20, -18}, {14, -18}, {14, 4}}, color = {0, 0, 255}));
  connect(inductor.n, emf.p) annotation(
    Line(points = {{8, 40}, {14, 40}, {14, 24}}, color = {0, 0, 255}));
  connect(resistor.n, inductor.p) annotation(
    Line(points = {{-20, 40}, {-12, 40}}, color = {0, 0, 255}));
  annotation(uses(Modelica(version = "3.2")),
    Diagram(coordinateSystem(extent = {{-150, -100}, {150, 100}})),
    Icon(coordinateSystem(extent = {{-150, -100}, {150, 100}})),
    version = "");
end dc_motor_with_velocity_feedback;