model Furutovo_kyvadlo
  inner Modelica.Mechanics.MultiBody.World world annotation(
    Placement(visible = true, transformation(origin = {-60, -14}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Mechanics.MultiBody.Joints.Revolute revolute2(phi(displayUnit = "rad",fixed = true, start = -1.19626), useAxisFlange = true, w(fixed = true))  annotation(
    Placement(visible = true, transformation(origin = { 20, 42}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Mechanics.MultiBody.Joints.Revolute revolute3(phi(displayUnit = "rad",fixed = true, start = 0.174533), useAxisFlange = true, w(fixed = true))  annotation(
    Placement(visible = true, transformation(origin = {68, 2}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Mechanics.Rotational.Components.Damper damper2(d = 0.1)  annotation(
    Placement(visible = true, transformation(origin = {22, 78}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Mechanics.Rotational.Components.Damper damper3(d = 0.1)  annotation(
    Placement(visible = true, transformation(origin = {66, 40}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Mechanics.MultiBody.Joints.Revolute revolute1(n = {0, 1, 0}, phi(displayUnit = "rad"), useAxisFlange = true)  annotation(
    Placement(visible = true, transformation(origin = {-34, 20}, extent = {{-10, -10}, {10, 10}}, rotation = 90)));
  Modelica.Mechanics.MultiBody.Parts.BodyBox bodyBox1(r = {0, 0, 0.3})  annotation(
    Placement(visible = true, transformation(origin = {-12, 42}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Mechanics.MultiBody.Parts.BodyBox bodyBox2(r = {1, 0, 0})  annotation(
    Placement(visible = true, transformation(origin = {42, 22}, extent = {{-10, -10}, {10, 10}}, rotation = -90)));
  Modelica.Mechanics.MultiBody.Parts.BodyBox bodyBox3(r = {1, 0, 0})  annotation(
    Placement(visible = true, transformation(origin = {82, -30}, extent = {{-10, -10}, {10, 10}}, rotation = -90)));
  Modelica.Mechanics.Rotational.Sources.Torque torque1 annotation(
    Placement(visible = true, transformation(origin = {-64, 22}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Blocks.Sources.Step step1 annotation(
    Placement(visible = true, transformation(origin = {-102, 22}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
equation
  connect(step1.y, torque1.tau) annotation(
    Line(points = {{-91, 22}, {-76, 22}}, color = {0, 0, 127}));
  connect(revolute3.axis, damper3.flange_b) annotation(
    Line(points = {{68, 12}, {76, 12}, {76, 40}}));
  connect(revolute3.support, damper3.flange_a) annotation(
    Line(points = {{62, 12}, {56, 12}, {56, 40}}));
  connect(torque1.flange, revolute1.axis) annotation(
    Line(points = {{-54, 22}, {-44, 22}, {-44, 20}}));
  connect(torque1.support, revolute1.support) annotation(
    Line(points = {{-64, 12}, {-46, 12}, {-46, 14}, {-44, 14}}));
  connect(world.frame_b, revolute1.frame_a) annotation(
    Line(points = {{-50, -14}, {-34, -14}, {-34, 10}}, color = {95, 95, 95}));
  connect(revolute1.frame_b, bodyBox1.frame_a) annotation(
    Line(points = {{-34, 30}, {-34, 42}, {-22, 42}}, color = {95, 95, 95}));
  connect(bodyBox1.frame_b, revolute2.frame_a) annotation(
    Line(points = {{-2, 42}, {10, 42}}, color = {95, 95, 95}));
  connect(revolute3.frame_b, bodyBox3.frame_a) annotation(
    Line(points = {{78, 2}, {82, 2}, {82, -20}, {82, -20}}, color = {95, 95, 95}));
  connect(bodyBox2.frame_b, revolute3.frame_a) annotation(
    Line(points = {{42, 12}, {44, 12}, {44, 2}, {58, 2}, {58, 2}}, color = {95, 95, 95}));
  connect(revolute2.frame_b, bodyBox2.frame_a) annotation(
    Line(points = {{30, 42}, {42, 42}, {42, 32}, {42, 32}}, color = {95, 95, 95}));
  connect(revolute2.support, damper2.flange_a) annotation(
    Line(points = {{14, 52}, {14, 52}, {14, 52}, {14, 52}, {14, 78}, {14, 78}, {14, 78}, {13, 78}, {13, 78}, {12, 78}}));
  connect(revolute2.axis, damper2.flange_b) annotation(
    Line(points = {{20, 52}, {27, 52}, {27, 52}, {34, 52}, {34, 78}, {34, 78}, {34, 78}, {33, 78}, {33, 78}, {32, 78}}));
  annotation(uses(Modelica(version = "3.2.2")),
    Diagram(coordinateSystem(extent = {{-150, -100}, {150, 100}})),
    Icon(coordinateSystem(extent = {{-150, -100}, {150, 100}})),
    version = "");
end Furutovo_kyvadlo;