model dc_motor 
  Modelica.Electrical.Analog.Basic.Ground ground annotation(Placement(transformation(extent = {{-20,0},{0,20}})));
  Modelica.Electrical.Analog.Basic.EMF emf(k = 0.29) annotation(Placement(visible = true, transformation(extent = {{18, 42}, {38, 62}}, rotation = 0)));
  Modelica.Electrical.Analog.Basic.Inductor inductor(L = 0.06) annotation(Placement(transformation(extent = {{-2,68},{18,88}})));
  Modelica.Electrical.Analog.Basic.Resistor resistor(R = 18) annotation(Placement(transformation(extent = {{-30,68},{-10,88}})));
  Modelica.Mechanics.Rotational.Components.Inertia rotor(J = 0.00086564) annotation(Placement(visible = true, transformation(extent = {{42, 42}, {62, 62}}, rotation = 0)));
  Modelica.Blocks.Sources.Step step1(startTime = 1)  annotation(
    Placement(visible = true, transformation(origin = {-70, 50}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Electrical.Analog.Sources.SignalVoltage signalVoltage1 annotation(
    Placement(visible = true, transformation(origin = {-38, 50}, extent = {{10, -10}, {-10, 10}}, rotation = 90)));
equation
  connect(ground.p, signalVoltage1.n) annotation(
    Line(points = {{-10, 20}, {-38, 20}, {-38, 40}, {-38, 40}}, color = {0, 0, 255}));
  connect(signalVoltage1.p, resistor.p) annotation(
    Line(points = {{-38, 60}, {-38, 60}, {-38, 78}, {-30, 78}, {-30, 78}}, color = {0, 0, 255}));
  connect(step1.y, signalVoltage1.v) annotation(
    Line(points = {{-58, 50}, {-45, 50}}, color = {0, 0, 127}));
  connect(emf.flange, rotor.flange_a) annotation(
    Line(points = {{38, 52}, {42, 52}}));
  connect(ground.p, emf.n) annotation(
    Line(points = {{-10, 20}, {28, 20}, {28, 42}}, color = {0, 0, 255}));
  connect(inductor.n, emf.p) annotation(
    Line(points = {{18, 78}, {28, 78}, {28, 62}}, color = {0, 0, 255}));
  connect(resistor.n, inductor.p) annotation(
    Line(points = {{-10, 78}, {-2, 78}}, color = {0, 0, 255}, smooth = Smooth.None));
  annotation(uses(Modelica(version = "3.2")), Diagram(graphics));
end dc_motor;