model hmotny_bod_na_pruzine_a_tlumici
  Modelica.Mechanics.Translational.Components.Fixed okolni_svet annotation(Placement(visible = true, transformation(origin = {1.0861, 61.985}, extent = {{-10, -10}, {10, 10}}, rotation = 180)));
  Modelica.Mechanics.Translational.Components.Spring pruzina(c = 1) annotation(Placement(visible = true, transformation(origin = {-17.8277, 32.3521}, extent = {{10, -10}, {-10, 10}}, rotation = -90)));
  Modelica.Mechanics.Translational.Components.Damper tlumic(d = 1) annotation(Placement(visible = true, transformation(origin = {19.7978, 34.1873}, extent = {{10, -10}, {-10, 10}}, rotation = -90)));
  Modelica.Mechanics.Translational.Components.Mass hmotny_bod(m = 1) annotation(Placement(visible = true, transformation(origin = {1.1161, -11.5431}, extent = {{10, -10}, {-10, 10}}, rotation = -90)));
  Modelica.Mechanics.Translational.Sources.Force sila annotation(Placement(visible = true, transformation(origin = {0.576801, -45.9176}, extent = {{-10, -10}, {10, 10}}, rotation = 90)));
  Modelica.Blocks.Sources.Step prubeh_sily(height = -1, startTime = 1)  annotation(Placement(visible = true, transformation(origin = {-32, -68}, extent = {{10, 10}, {-10, -10}}, rotation = 180)));
equation
  connect(prubeh_sily.y, sila.f) annotation(
    Line(points = {{-21, -68}, {1, -68}, {1, -58}}, color = {0, 0, 127}));
  connect(pruzina.flange_b, okolni_svet.flange) annotation(
    Line(points = {{-18, 42}, {-18, 62}, {2, 62}}, color = {0, 127, 0}));
  connect(tlumic.flange_b, okolni_svet.flange) annotation(
    Line(points = {{20, 44}, {20, 62}, {2, 62}}, color = {0, 127, 0}));
  connect(hmotny_bod.flange_b, pruzina.flange_a) annotation(
    Line(points = {{1, -2}, {-18, -2}, {-18, 22}}, color = {0, 127, 0}));
  connect(hmotny_bod.flange_b, tlumic.flange_a) annotation(
    Line(points = {{1, -2}, {20, -2}, {20, 24}}, color = {0, 127, 0}));
  connect(sila.flange, hmotny_bod.flange_a) annotation(
    Line(points = {{1, -36}, {1, -22}}, color = {0, 127, 0}));
  annotation(
    Icon(coordinateSystem(extent = {{-100, -100}, {100, 100}}, preserveAspectRatio = true, initialScale = 0.1, grid = {2, 2})),
    Diagram(coordinateSystem(extent = {{-100, -100}, {100, 100}}, preserveAspectRatio = true, initialScale = 0.1, grid = {2, 2})),
    uses(Modelica(version = "3.2.3")));
end hmotny_bod_na_pruzine_a_tlumici;